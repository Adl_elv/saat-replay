# Saat Replay

## Description
Saat Replay est un logiciel conçu pour reproduire en temps réel les archives sur l'affichage du SAAT (Système d'Aide à l'Affichage et au Traitement). Ce projet permet un suivi précis et dynamique des trains, utilisant les paramétrages de SAAT et des archives pour simuler l'affichage.

## Installation
Pour installer Saat Replay, veuillez télécharger le fichier **"Saat Replay Utilisateur"** correspondant à la version souhaitée depuis les [releases sur GitLab](https://gitlab.com/Adl_elv/saat-replay/-/releases). Notez que la version "Saat Replay Developper" est réservée aux experts techniques.

## Utilisation
1. Placez l'exécutable dans le dossier de paramétrage du SAAT.
2. Ouvrez le logiciel.
3. (Optionnel) Sélectionnez une archive en haut de la page.
4. Chargez l'archive en utilisant la loupe.
5. Il est possible d'enregistrer l'archive, d'appliquer des critères et conditions (ET/OU), et de sélectionner une ligne spécifique dans l'archive pour débuter à un moment donné.
6. Valider pour lancer l'affichage du SAAT.
7. Si une archive est chargée, utilisez la flèche de droite pour parcourir l'archive en temps réel.

## Signalement de Problèmes
Pour tout problème ou question, veuillez utiliser la section de report de bug sur GitLab ou me contacter directement à mon e-mail.

## Technologies
- **Langage de Programmation**: Python
- **Interface Graphique**: Qt Designer

## Status du projet
En cours d'amélioration, toujours en développement.

## Contribution
Ce projet est actuellement géré par moi-même, Adlane Gil (adlane.gil@icloud.com). Pour toute contribution ou question, n'hésitez pas à me contacter directement.

## Licence
Le logiciel est à usage interne uniquement.

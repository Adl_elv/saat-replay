import sys
from PySide6.QtGui import QShortcut, QKeySequence, Qt
from PySide6.QtWidgets import QApplication
from module.FileWindow import FileDialog
from module.Gestion import suivant, precedent, rotate
from module.Counter import Counter
from module.SupressionPile import SupressionPile
from module.FileManager import lecture_archive, lecture_saat_ext, definition_carac, lecture_trame, definition_fenetres
from module.Dialog_choix_archive import DialogArchive

import os

from module.Start_menu import StartMenu


def find_file(folder_path, file_name):
    for root, dirs, files in os.walk(folder_path):
        if file_name in files:
            return os.path.join(root, file_name)
    return None


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Initialiser l'application
    app = QApplication(sys.argv)

    # Initialiser le compteur
    index = Counter()

    # Initialiser la pile de supression
    supression_pile = SupressionPile()

    # Charger les données de l'archive
    start_menu = StartMenu()

    app.exec()

    DialogArchive().setupUi(start_menu)

    # Charge les données des paramètres
    file_archive = FileDialog()
    file_archive.show_dialog_param()
    dir_path = file_archive.file_name
    # chemin_dossier_courant = os.getcwd()
    # dir_path = chemin_dossier_courant

    # Obtenir les données et la fenêtre de Saat externe
    dict_saat_externe, fenetre_saat_externe = lecture_saat_ext(dir_path)
    # Obtenir les caractéristiques
    list_carac = definition_carac(dir_path)

    # Créer les fenêtres de Saat interne à partir de la trame
    list_fenetre_saat_interne = lecture_trame(dir_path, list_carac)

    # Définir les fenêtres de train
    list_fenetre_train = definition_fenetres(dir_path, list_fenetre_saat_interne)

    # Configurer les raccourcis clavier pour la fenêtre de Saat externe
    shortcut_next = QShortcut(QKeySequence(Qt.Key_Right), fenetre_saat_externe)
    shortcut_next.activated.connect(
        lambda: suivant(index, list_fenetre_saat_interne, fenetre_saat_externe, start_menu.archive, dict_saat_externe,
                        list_fenetre_train, supression_pile))

    shortcut_prev = QShortcut(QKeySequence(Qt.Key_Left), fenetre_saat_externe)
    shortcut_prev.activated.connect(
        lambda: precedent(index, list_fenetre_saat_interne, fenetre_saat_externe, start_menu.archive, dict_saat_externe,
                          list_fenetre_train, supression_pile))

    shortcut_rotate = QShortcut(QKeySequence(Qt.Key_Space), fenetre_saat_externe)
    shortcut_rotate.activated.connect(lambda: rotate(list_fenetre_saat_interne, list_fenetre_train))

    # Début de l'archive a la ligne séléctionner
    while index.value < start_menu.selected_index - 1:
        suivant(index, list_fenetre_saat_interne, fenetre_saat_externe, start_menu.archive, dict_saat_externe,
                list_fenetre_train, supression_pile)

    # Configurer les raccourcis clavier pour chaque fenêtre de Saat interne
    for window in list_fenetre_saat_interne:
        shortcut_next = QShortcut(QKeySequence(Qt.Key_Right), window)
        shortcut_next.activated.connect(
            lambda: suivant(index, list_fenetre_saat_interne, fenetre_saat_externe, start_menu.archive,
                            dict_saat_externe,
                            list_fenetre_train, supression_pile))

        shortcut_prev = QShortcut(QKeySequence(Qt.Key_Left), window)
        shortcut_prev.activated.connect(
            lambda: precedent(index, list_fenetre_saat_interne, fenetre_saat_externe, start_menu.archive,
                              dict_saat_externe, list_fenetre_train, supression_pile))

        shortcut_rotate = QShortcut(QKeySequence(Qt.Key_Space), window)
        shortcut_rotate.activated.connect(lambda: rotate(list_fenetre_saat_interne, list_fenetre_train))

    # Exécuter l'application
    app.exec()

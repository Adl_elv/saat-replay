from datetime import datetime


class LineConditionChecker:
    def __init__(self, index, valeur, condition_type="ou"):
        """
        Initializes a condition checker for a line with specified index, value, and condition type.

        Args:
            index (int): The index of the field to check in the line.
            valeur (str): The value to check against the field.
            condition_type (str): The type of condition ('ou' for OR, 'et' for AND).
        """
        self.index = index
        self.valeur = valeur
        self.condition_type = condition_type

    def champs_line(self, line):
        """
        Extracts the field from the line based on the specified index.

        Args:
            line (object): The line object from which to extract the field.

        Returns:
            str: The extracted field as a string.
        """
        if self.index == 0:
            return str(line)
        elif self.index == 1 or self.index == 2:
            return line.date + " " + line.heure
        elif self.index == 3 or self.index == 4:
            return line.code_alarme
        elif self.index == 5:
            return line.sens_message
        elif self.index == 6:
            return line.num_processeur + " " + line.num_liaison_serie
        elif self.index == 7:
            return line.carac_repetition
        elif self.index == 8:
            return line.code_message
        elif self.index == 9:
            return line.lib_message

    def check_line_et(self, line):
        """
        Checks if the line meets the conditions using the 'et' (AND) logic.

        Args:
            line (object): The line object to check.

        Returns:
            bool: True if the line meets the conditions, False otherwise.
        """
        line_val = self.champs_line(line)
        if self.index == 1:
            if not is_before(self.valeur, line_val):
                return False
        elif self.index == 2:
            if not is_after(self.valeur, line_val):
                return False
        elif self.index == 3:
            if self.valeur:
                if not line_val.strip():
                    return False
            else:
                if line_val.strip():
                    return False
        elif self.valeur not in line_val:
            return False
        return True

    def check_line_ou(self, line):
        """
        Checks if the line meets the conditions using the 'ou' (OR) logic.

        Args:
            line (object): The line object to check.

        Returns:
            bool: True if the line meets the conditions, False otherwise.
        """
        line_val = self.champs_line(line)
        if self.index == 1:
            if is_before(self.valeur, line_val):
                return True
        elif self.index == 2:
            if is_after(self.valeur, line_val):
                return True
        elif self.index == 3:
            if self.valeur:
                if line_val.strip():
                    return True
            else:
                if not line_val.strip():
                    return True
        elif self.valeur in line_val:
            return True
        return False


def is_before(date1, date2):
    """
    Checks if date1 is before or equal to date2.

    Args:
        date1 (str): The first date in the format 'dd/mm/yy HH:MM:SS'.
        date2 (str): The second date, in either 'dd/mm/yy HH/MM/SS' or 'dd/mm/yy HHhMMmSSs' format.

    Returns:
        bool: True if date1 is before or equal to date2, False otherwise.
    """
    # Define the format of the date
    date1_format = "%d/%m/%y %H:%M:%S"
    date1 = datetime.strptime(date1, date1_format)

    # For date2, try the 2 possible format
    try:
        date2_format = "%d/%m/%y %H/%M/%S"
        date2 = datetime.strptime(date2, date2_format)
    except ValueError:
        # If the first format fail, try the second one
        date2_format = "%d/%m/%y %HH%MMn%SSs"
        date2 = datetime.strptime(date2, date2_format)

    # Compare the 2 dates and return True if date1 is before date2 False if not
    return date1 <= date2


def is_after(date1, date2):
    """
    Checks if date1 is after or equal to date2.

    Args:
        date1 (str): The first date in the format 'dd/mm/yy HH:MM:SS'.
        date2 (str): The second date, in either 'dd/mm/yy HH/MM/SS' or 'dd/mm/yy HHhMMmSSs' format.

    Returns:
        bool: True if date1 is after or equal to date2, False otherwise.
    """
    # Define the format of the date
    date1_format = "%d/%m/%y %H:%M:%S"
    date1 = datetime.strptime(date1, date1_format)

    # For date2, try the 2 possible format
    try:
        date2_format = "%d/%m/%y %H/%M/%S"
        date2 = datetime.strptime(date2, date2_format)
    except ValueError:
        # If the first format fail, try the second one
        date2_format = "%d/%m/%y %HH%MMn%SSs"
        date2 = datetime.strptime(date2, date2_format)

    # Compare the 2 dates and return True if date1 is before date2 False if not
    return date1 >= date2

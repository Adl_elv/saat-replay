import sys
import os


def resource_path(relative_path):
    """
    Gets the absolute path to a resource, works for both development and PyInstaller packaging.

    Arguments:
        relative_path (string): The relative path to the resource.

    Returns:
        string: The absolute path to the resource.
    """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, "images/" + relative_path)

import re


class Identificateur:
    """
    The Identifier class is used to extract and store information from a specific line of data.

    Attributes:
        date (str): La date de l'identificateur.
        heure (str): L'heure de l'identificateur.
        code_alarme (str): Le code d'alarme de l'identificateur.
        sens_message (str): Le sens du message ('E' pour émission, 'R' pour réception).
        num_processeur (str): Le numéro du processeur de l'identificateur.
        num_laison_serie (str): Le numéro de la liaison série de l'identificateur.
        num_message (str): Le numéro du message de l'identificateur.
        carac_repetition (str): Le caractère de répétition de l'identificateur.
        code_message (str): Le code du message de l'identificateur.
        lib_message (str): Le libellé du message de l'identificateur, avec les espaces et les sauts de ligne
                            supprimés.
    """
    def __init__(self, ligne):
        """
        Initializes an Identifier object with values extracted from the given line.

        Args:
            ligne (str): A data line containing the information of the identifier.
        """
        # Split the line into variables using spaces as separators
        variables_brut = re.split(r'( +)', ligne)

        variables = []

        # Use list comprehension to eliminate unnecessary space strings
        for elem in variables_brut:
            if elem.isspace():  # Check if the string is only spaces
                if len(elem) > 2:  # If the string has more than two spaces
                    elem = elem[2:]  # Remove the first two spaces
                else:  # If the string has one or two spaces
                    continue  # Skip adding this element to the list
            variables.append(elem)
        self.date = variables[0]
        self.heure = variables[1]
        self.code_alarme = variables[2]
        self.sens_message = variables[3]
        self.num_processeur = variables[4]
        self.num_liaison_serie = variables[5]
        self.num_message = variables[6]
        self.carac_repetition = variables[7]
        self.code_message = variables[8]
        self.lib_message = ' '.join(variables[9:]).strip("\n")

        # Check specific conditions for modifying the message label
        if len(variables[len(variables) - 1]) > 6:
            self.lib_message = self.lib_message[:-1]

        elif (self.sens_message == 'E' and self.code_alarme == "   " and self.carac_repetition != '0') \
                or (self.sens_message == 'R' and self.code_message == 'H'):
            self.lib_message = self.lib_message[:-1]

    def __str__(self):
        """
        Returns a string representation of the Identifier object.

        Returns:
            str: The string representation of the Identifier.
        """
        return (f"{self.date} {self.heure} {self.code_alarme} {self.sens_message} {self.num_processeur}"
                f" {self.num_liaison_serie} {self.num_message} {self.carac_repetition} {self.code_message}"
                f" {self.lib_message}")

    def get_field_by_index(self, index):
        """
         Returns the value of a field of the Identifier object based on the given index.

         Args:
             index (int): The index of the field to retrieve.

         Returns:
             The value of the field corresponding to the given index.

         Raises:
             ValueError: If the index is out of bounds.
         """
        fields = ['date', 'heure', 'code_alarme', 'sens_message', 'num_processeur', 'num_liaison_serie',
                  'num_message', 'carac_repetition', 'code_message', 'lib_message']
        if index < len(fields):
            return getattr(self, fields[index])
        else:
            raise ValueError("Index hors des limites.")

            
import sys
from PySide6 import QtCore
from PySide6.QtCore import QPropertyAnimation
from PySide6.QtGui import QKeyEvent
from PySide6.QtWidgets import QMainWindow
from module.ui_SAATExtWindow import Ui_MainWindow
from functools import partial


class SAATExtWindow(QMainWindow):
    """
    Class representing the external SAAT window.

    Attributes:
        ui (Ui_MainWindow): The user interface of the window.
        animation (QPropertyAnimation): The animation used for showing/hiding the side menu.

    Methods:
        __init__(self, saat_ext): Initializes a new instance of the SAATExtWindow class.
        toggle_menu(): Shows or hides the side menu.
        highlight_button(index): Highlights the clicked button and displays the corresponding page.
        closeEvent(event): Handles the window closing event.
        keyPressEvent(event): Handles the key press event.
    """

    def __init__(self, saat_ext):
        """
        Initializes a new instance of the SAATExtWindow class.

        Args:
            saat_ext: The saat_ext object passed to the user interface to initialize the configuration.

        Returns:
            None
        """
        QMainWindow.__init__(self)  # Initialize the QMainWindow base class
        self.ui = Ui_MainWindow()  # Create a new instance of the UI
        self.ui.setupUi(self, saat_ext)  # Set up the UI with the saat_ext object
        self.animation = None  # Initialize animation as None

        # Connect the toggle button to the toggle_menu function
        self.ui.btn_toggle.clicked.connect(lambda: self.toggle_menu())

        # Connect each button in the UI to the highlight_button function
        # Using 'partial' to pass the index as a parameter to the highlight_button function
        for index, element in enumerate(self.ui.content):
            element[0].clicked.connect(partial(self.highlight_button, index))

        self.show()  # Display the window

    def toggle_menu(self):
        """
        Shows or hides the side menu with an animation.

        Returns:
            None
        """
        # Create a property animation for the left menu frame's minimum width
        self.animation = QPropertyAnimation(self.ui.frame_left_menu, b"minimumWidth")
        self.animation.setDuration(400)  # Animation duration in milliseconds
        # Set the easing curve for the animation to smoothly open and close
        self.animation.setEasingCurve(QtCore.QEasingCurve.InOutQuart)

        # Set the start and end values of the animation
        # If the menu width is 70, animate to expand to 250, otherwise animate to shrink to 70
        if self.ui.frame_left_menu.width() == 70:
            self.animation.setStartValue(70)
            self.animation.setEndValue(250)
        else:
            self.animation.setStartValue(250)
            self.animation.setEndValue(70)

        self.animation.start()  # Start the animation

    def highlight_button(self, index):
        """
        Highlights the clicked button and displays the corresponding page.

        Args:
            index (int): The index of the clicked button.

        Returns:
            None
        """
        # Reset all buttons to their normal state
        for element in self.ui.content:
            element[0].setStyleSheet(u"QPushButton {\n"
                                     "	color: rgb(255, 255, 255);\n"
                                     "	background-color: rgb(35, 35, 35);\n"
                                     "	border: 0px solid;\n"
                                     "}\n"
                                     "QPushButton:hover {\n"
                                     "	background-color: rgb(85, 170, 255);\n"
                                     "}")

        # Highlight the clicked button
        self.ui.content[index][0].setStyleSheet(u"QPushButton {\n"
                                                "	color: rgb(255, 255, 255);\n"
                                                "	background-color: rgb(85, 170, 255);\n"
                                                "	border: 0px solid;\n"
                                                "}\n")

        # Change the currently displayed page in the stacked widget
        self.ui.stackedWidget.setCurrentWidget(self.ui.content[index][1])

    def closeEvent(self, event):
        """
        Handles the window closing event.

        Args:
            event: The close event.

        Returns:
            None
        """
        # Accept the close event to ensure the application closes smoothly
        event.accept()
        # Exit the application with a status code of 0 (indicating successful termination)
        sys.exit(0)

    def keyPressEvent(self, event: QKeyEvent):
        """
        Handles the key press event.

        Args:
            event (QKeyEvent): The key press event.

        Returns:
            None
        """
        # Call the base class implementation to ensure that key press events are handled appropriately
        super().keyPressEvent(event)


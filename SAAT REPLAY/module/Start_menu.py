import re
import sys
from PySide6.QtCore import QCoreApplication
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QMainWindow, QLineEdit, QFileDialog

from module.Condition import LineConditionChecker
from module.FileManager import lecture_archive
from module.FileWindow import FileDialog
from module.Identificateur import Identificateur
from module.InformationWindow import Information
from module.ui_StartMenu import Ui_MainWindow
from module.FilePath import resource_path


class StartMenu(QMainWindow):
    """
    Class representing the start menu window for external SAAT configuration.

    Attributes:
        ui (Ui_MainWindow): The user interface of the window.

    Methods:
        __init__(self, saat_ext): Initializes a new instance of the StartMenu class.
        toggle_menu(): Shows or hides the side menu.
        highlight_button(index): Highlights the clicked button and displays the corresponding page.
        closeEvent(event): Handles the window closing event.
        keyPressEvent(event): Handles the key press event.
    """

    def __init__(self):
        """
        Initializes a new instance of the StartMenu class.

        Args:

        Returns:
            None
        """
        QMainWindow.__init__(self)  # Initialize the QMainWindow base class
        self.ui = Ui_MainWindow()  # Create a new instance of the UI
        self.ui.setupUi(self)  # Setup the UI for the window

        self.ui.dialog_selection.set_menu(self)

        # Connect buttons to their respective event handling functions
        self.ui.pushButton_1.clicked.connect(lambda: self.search_event())
        self.ui.pushButton_2.clicked.connect(lambda: self.validate_event())
        self.ui.pushButton_3.clicked.connect(lambda: self.save_event())
        self.ui.pushButton_3.hide()

        # Connect logical operation buttons to their respective functions
        self.ui.et.clicked.connect(lambda: self.add_condition("et"))
        self.ui.ou.clicked.connect(lambda: self.add_condition("ou"))

        self.info = []  # Initialize a list to store information
        self.index = []  # Initialize a list to store index values
        self.archive = []  # Initialize a list to store archive data
        self.archive_filtered = []  # Initialize a list to store archive data filtered
        self.selected_index = 0  # Initialize the selected index

        self.show()  # Display the window

    def validate_event(self):
        """
        Validates the current selection in the list widget and updates the selected index accordingly.

        This method attempts to update `selected_index` based on the user's selection in the list widget. If no item is
        selected, or if an IndexError occurs due to an empty selection, it defaults to the first item in the list if available.
        After updating the selection or handling an empty selection, it hides the current window and quits the application.

        Args:
            None

        Returns:
            None
        """
        try:
            # Attempt to get the selected indexes from the list widget
            indexes = self.ui.list_widget.selectedIndexes()
            # Update the selected index based on the first selected item
            self.selected_index = self.index[indexes[0].row()]
        except IndexError:
            # Handle cases where no selection is made by defaulting to the first item, if available
            try:
                self.selected_index = self.index[0]
            except IndexError:
                # If the index list is empty, do nothing
                None
        # Hide the current window and quit the application
        self.hide()
        QCoreApplication.quit()

    def search_event(self):
        """
        Searches through the archive data based on conditions defined in the user interface,
        updates the list widget with the search results, and stores the corresponding indexes.

        This method clears the current contents of the list widget and resets the index tracking list.
        It then loads data from the archive specified by the user in the dialog selection input.
        Each line of the archive is processed to remove non-printable characters and is then converted
        into an `Identificateur` object. The method evaluates each line against specified conditions
        (either 'and' or 'or' conditions). If a line meets any of the 'or' conditions or all of the 'and' conditions,
        it is added to the list widget, and its index is stored.

        Args:
            None

        Returns:
            None
        """
        # Clear current items from the QListWidget
        self.ui.list_widget.clear()
        self.index = []
        self.archive_filtered = []
        # Load archive data
        self.archive = lecture_archive(self.ui.dialog_selection.text())
        conditions = self.condition_list()
        cmpt = -1
        self.ui.pushButton_3.show()
        for line in self.archive:
            try:
                cmpt += 1
                # Clean the line from non-printable characters
                line = re.sub(r'[\x00-\x1F\x7F-\x9F\ufeff\xef\xbb\xbf]', ' ', line.rstrip("\n").strip()).strip()
                line_id = Identificateur(line)
                et_condition = []
                ou_condition = []
                # Evaluate the line against 'and' and 'or' conditions
                for condition in conditions:
                    if condition.condition_type == "et":
                        et_condition.append(condition.check_line_et(line_id))
                    else:
                        ou_condition.append(condition.check_line_ou(line_id))
                # If the line meets the conditions, add it to the list widget and store its index
                if (all(et_condition) and et_condition) or (any(ou_condition) and ou_condition):
                    self.index.append(cmpt)
                    self.archive_filtered.append(self.archive[cmpt])
                    self.ui.list_widget.addItem(str(line_id))
            except IndexError:
                continue

    def save_event(self):
        """
        Initiates the process to save the current list widget contents to a file.

        This method creates and displays a file dialog that allows the user to select a location and specify a file
        name for saving the contents of the list widget. The selected data from the list widget,
        typically search results or filtered data, is then saved to the specified file.

        Args:
            None

        Returns:
            None
        """
        # Create and display a file dialog to save the file
        file_dialog = FileDialog()
        file_dialog.show_dialog_select_file(self.ui.list_widget)

    def add_condition(self, condition_type):
        # Determine the index for the new condition
        if self.info:
            index = self.info[-1].index + 1
        else:
            index = 2
            if condition_type == "et":
                self.ui.ou.hide()
            else:
                self.ui.et.hide()

        # Create a new 'and' condition at the determined index
        self.info.append(Information(self.ui.formulaire, condition_type, index))
        # Add the new condition to the UI's vertical layout
        self.ui.verticalLayout_6.addWidget(self.info[-1].information)
        # Connect the condition's delete button to allow for its removal
        self.info[-1].poubelle.clicked.connect(lambda: self.poubelle_clicked(index))
        chemin_svg = resource_path("trash.svg")
        self.info[-1].poubelle.setIcon(QIcon(chemin_svg))



    def poubelle_clicked(self, index):
        """
        Handles the deletion of a condition field when its corresponding delete button is clicked.

        This method iterates through the existing condition fields (represented as elements in the `info` list).
        It identifies the condition field with the matching index and deletes its UI representation.
        The method then updates the `info` list to exclude the deleted condition, effectively removing it from both
        the user interface and the application's record of active conditions.

        Args:
            index (int): The index of the condition field to be deleted.

        Returns:
            None
        """
        # Iterate through the condition fields to find the one with the matching index
        for element in self.info:
            if element.index == index:
                # Delete the UI representation of the condition field
                element.information.deleteLater()
        # Update the `info` list to exclude the deleted condition
        self.info = [element for element in self.info if element.index != index]
        if len(self.info) == 0:
            self.ui.ou.show()
            self.ui.et.show()

    def condition_list(self):
        """
        Compiles a list of condition checkers based on user-defined criteria for filtering or searching.

        This method initializes a list of condition checkers by examining the user inputs specified in the UI.
        It determines the type of condition (date/time or text-based) based on the selected index and captures the
        corresponding value. The first condition is added based on the primary UI elements outside of the dynamically
        added conditions (`self.info`). Subsequent conditions are added for each element within `self.info`, creating a
        comprehensive list of condition checkers that reflect all user-defined criteria.

        Args:
            None

        Returns:
            list: A list of LineConditionChecker instances, each representing a condition defined by the user.
        """
        condition = []
        # Determine the type of the primary condition and capture its value
        index = self.ui.choix_index
        if index == 1 or index == 2:
            valeur = self.ui.date.dateTime().toString("dd/MM/yy HH:mm:ss")
        elif index == 3:
            valeur = self.ui.checkbox.isChecked()
        else:
            valeur = self.ui.text.text()
        # Add the primary condition to the list
        if len(self.info) > 0:
            condition.append(LineConditionChecker(index, valeur, self.info[0].operateur))
        else:
            condition.append(LineConditionChecker(index, valeur))
        # Add additional conditions from `self.info`
        for condition_elem in self.info:
            index = condition_elem.choix_index
            if index == 1 or index == 2:
                valeur = condition_elem.date.dateTime().toString("dd/MM/yy HH:mm:ss")
            elif index == 3:
                valeur = condition_elem.checkbox.isChecked()
            else:
                valeur = condition_elem.text.text()
            condition_type = condition_elem.operateur
            condition.append(LineConditionChecker(index, valeur, condition_type))
        return condition

    def closeEvent(self, event):
        """
        Handles the window's closing event.

        This method is called when the window is about to close. It ensures that the application shuts down cleanly by accepting the close event and then calling `sys.exit()` to terminate the application.

        Args:
            event: The close event triggered by the window system.

        Returns:
            None
        """
        # Accept the close event to signal that the window closure has been handled
        event.accept()
        # Terminate the application
        sys.exit()


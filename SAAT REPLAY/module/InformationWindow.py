import os
import sys
from PySide6.QtCore import QSize, Qt, QCoreApplication
from PySide6.QtGui import QPalette, QBrush, QColor, QIcon
from PySide6.QtWidgets import QFrame, QHBoxLayout, QComboBox, QLineEdit, QPushButton, QStackedWidget, QDateTimeEdit, \
    QWidget, QLabel, QCheckBox


class Information:
    def __init__(self, formulaire, operateur, index):

        self.operateur = operateur
        self.index = index

        brush = QBrush(QColor(255, 255, 255, 255))
        brush.setStyle(Qt.SolidPattern)
        brush1 = QBrush(QColor(45, 45, 45, 255))
        brush1.setStyle(Qt.SolidPattern)

        self.information = QFrame(formulaire)
        self.information.setObjectName(u"information")
        self.information.setMaximumSize(QSize(16777215, 47))
        self.information.setFrameShape(QFrame.StyledPanel)
        self.information.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.information)
        self.horizontalLayout.setSpacing(10)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(10, 10, 10, 10)
        self.choix_index = 0
        self.choix_menu = QComboBox(self.information)
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.setObjectName(u"choix_menu")
        self.choix_menu.setMinimumSize(QSize(0, 0))
        self.choix_menu.setStyleSheet("""
        QComboBox QAbstractItemView {
            color: darkgray; 
        }
        """)
        self.choix_menu.currentIndexChanged.connect(self.on_combo_changed)

        palette = QPalette()
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.choix_menu.setPalette(palette)

        self.horizontalLayout.addWidget(self.choix_menu)

        self.choix_saisie = QStackedWidget(self.information)

        self.text = QLineEdit(self.information)
        self.text.setObjectName(u"text")
        self.text.setStyleSheet("color: white;")

        self.date = QDateTimeEdit(self.information)
        self.date.setObjectName(u"date")
        self.date.setStyleSheet("color: white;")

        self.alarmSelectionWidget = QWidget(self.information)
        self.alarmSelectionLayout = QHBoxLayout(self.alarmSelectionWidget)
        self.labelSelectAlarms = QLabel("Sélectionner les alarmes")
        self.labelSelectAlarms.setObjectName(u"alarm label")
        self.labelSelectAlarms.setStyleSheet("color: white;")
        self.checkbox = QCheckBox(self.information)
        self.checkbox.setObjectName(u"checkbox")
        self.checkbox.setStyleSheet("color: white;")
        self.alarmSelectionLayout.addWidget(self.labelSelectAlarms)
        self.alarmSelectionLayout.addWidget(self.checkbox)
        self.alarmSelectionLayout.setContentsMargins(0, 0, 0, 0)

        self.choix_saisie.addWidget(self.text)  # index 0
        self.choix_saisie.addWidget(self.date)  # index 1
        self.choix_saisie.addWidget(self.alarmSelectionWidget)  # index 2

        self.horizontalLayout.addWidget(self.choix_saisie)

        self.poubelle = QPushButton(self.information)
        self.poubelle.setObjectName(u"poubelle")
        icon = QIcon()
        # chemin_base = sys._MEIPASS
        # chemin_svg = os.path.join(chemin_base, "images", "trash.svg")
        chemin_svg = os.path.join("images", "trash.svg")
        icon.addFile(chemin_svg, QSize(), QIcon.Normal, QIcon.Off)
        self.poubelle.setIcon(icon)

        self.horizontalLayout.addWidget(self.poubelle)

        self.choix_menu.setItemText(0, QCoreApplication.translate("MainWindow", u"Tous", None))
        self.choix_menu.setItemText(1, QCoreApplication.translate("MainWindow", u"\u00c0 partir de", None))
        self.choix_menu.setItemText(2, QCoreApplication.translate("MainWindow", u"Jusqu'a", None))
        self.choix_menu.setItemText(3, QCoreApplication.translate("MainWindow", u"Alarme", None))
        self.choix_menu.setItemText(4, QCoreApplication.translate("MainWindow", u"Code alarme", None))
        self.choix_menu.setItemText(5, QCoreApplication.translate("MainWindow", u"Sens du message", None))
        self.choix_menu.setItemText(6, QCoreApplication.translate("MainWindow", u"Num\u00e9ro de liaison s\u00e9rie", None))
        self.choix_menu.setItemText(7, QCoreApplication.translate("MainWindow", u"Caract\u00e8re de r\u00e9p\u00e9tition", None))
        self.choix_menu.setItemText(8, QCoreApplication.translate("MainWindow", u"Type du message", None))
        self.choix_menu.setItemText(9, QCoreApplication.translate("MainWindow", u"Information du message", None))

        self.poubelle.setText("")

    def on_combo_changed(self, index):

        self.choix_index = index
        if index == 1 or index == 2:
            self.choix_saisie.setCurrentIndex(1)  # show QDateTimeEdit
        elif index == 3:
            self.choix_saisie.setCurrentIndex(2)  # show QCheckBox
        else:
            self.choix_saisie.setCurrentIndex(0)  # show QLineEdit

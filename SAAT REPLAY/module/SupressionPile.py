from collections import deque


class SupressionPile:
    """
    Class representing a stack for managing train deletions.
    """

    def __init__(self):
        """
        Initializes a new instance of the SupressionPile class.

        Returns:
            None
        """
        self.train_stocker = deque()  # Create a deque for storing train numbers
        self.taille = 300  # Maximum size of the stack

    def add(self, num_train):
        """
        Adds a train number to the stack. If the stack is full, it removes the oldest entry before adding.

        Args:
            num_train: The train number to add to the stack.

        Returns:
            None
        """
        if len(self.train_stocker) >= self.taille:
            self.train_stocker.popleft()  # Remove the oldest entry
        self.train_stocker.append(num_train)  # Add the new train number

    def sup(self):
        """
        Removes and returns the last train number from the stack.

        Returns:
            The last train number, or False if the stack is empty.
        """
        if len(self.train_stocker) <= 0:
            return False
        else:
            return self.train_stocker.pop()

from PySide6.QtCore import Qt
from PySide6.QtGui import QColor
from PySide6.QtWidgets import QTableWidgetItem

from module.Alarm import alarm_map
from module.End_menu import EndMenu
from module.Identificateur import Identificateur
import re

from module.Interpretation import id_to_action_int, id_to_action_int_dec, id_to_action_ext


def update_ui(id_ligne, list_fenetre_saat, line, color):
    """
    Updates the user interface for the main SAAT windows.

    Args:
        id_ligne (Identifier): Identifier of the line.
        list_fenetre_saat (list): List of SAAT windows.
        line (str): The string of the current line.
        color (str): The color of the string of the current line.
    """
    for fenetre in list_fenetre_saat:
        # Update the display of identification information
        fenetre.ui.label_2.setText("    " + id_ligne.date + "    " + id_ligne.heure)
        fenetre.ui.label_4.setText(alarm_map.get(id_ligne.code_alarme))
        fenetre.ui.label_5.setText(line)
        fenetre.ui.label_5.setStyleSheet('color: ' + color + '; font-size: 17px;')


def ajoute_ext(index, ligne, id_arg, fenetre_saat):
    """
    Adds a new line to the external SAAT window with the provided information.

    Args:
        index (int): The index of the line to add.
        ligne (str): The data of the line.
        id_arg (Identifier): Identifier of the line.
        fenetre_saat (SAAT): The external SAAT window.
    """
    # Prepare the elements to add to the table
    commande = QTableWidgetItem(ligne)
    commande_bis = QTableWidgetItem(ligne)
    action = QTableWidgetItem(id_to_action_ext(id_arg))
    error_color = QColor(Qt.red)
    normal_color = QColor(Qt.white)

    # Gray out previous items in the table
    for row in range(fenetre_saat.ui.content[0][2].rowCount()):
        item = fenetre_saat.ui.content[0][2].item(row, 1)
        if item:  # Ensure the item exists
            item.setForeground(QColor("gray"))

    # If an alarm is triggered, texts will be displayed in red
    if id_arg.code_alarme != "   ":
        commande.setForeground(error_color)
        commande_bis.setForeground(error_color)
        action.setForeground(error_color)
    else:
        commande.setForeground(normal_color)
        commande_bis.setForeground(normal_color)
        action.setForeground(normal_color)

    # Add a new line and insert the elements
    ligne_index = fenetre_saat.ui.content[index][2].rowCount()
    fenetre_saat.ui.content[index][2].insertRow(ligne_index)
    fenetre_saat.ui.content[index][2].setItem(ligne_index, 0, commande)
    fenetre_saat.ui.content[index][2].setItem(ligne_index, 1, action)
    fenetre_saat.ui.content[0][2].setItem(index - 1, 1, commande_bis)


def supprime_ext(index, fenetre_saat):
    """
    Deletes the last line in the external SAAT window.

    Args:
        index (int): The index of the line to delete.
        fenetre_saat (SAAT): The external SAAT window.
    """
    # Gray out previous items in the table
    for row in range(fenetre_saat.ui.content[0][2].rowCount()):
        item = fenetre_saat.ui.content[0][2].item(row, 1)
        if item:  # Ensure the item exists
            item.setForeground(QColor("gray"))

    # Check if the table contains any rows
    if fenetre_saat.ui.content[index][2].rowCount() > 0:
        # Remove the last row from the table
        fenetre_saat.ui.content[index][2].removeRow(fenetre_saat.ui.content[index][2].rowCount() - 1)

    # Clear the text of the last item in the first row
    fenetre_saat.ui.content[0][2].item(index - 1, 1).setText("")


def update_external(id_ligne, line, dictionnaire_saat_externe, fenetre_saat_externe):
    """
    Updates the external SAAT.

    Args:
        id_ligne (Identifier): Identifier of the line.
        line (str): The string of the current line.
        dictionnaire_saat_externe (dict): The external SAAT dictionary.
        fenetre_saat_externe (SAAT): The external SAAT window.
    """
    # Iterate through the external SAAT dictionary to find a matching entry
    for i, key in enumerate(dictionnaire_saat_externe, start=1):
        if dictionnaire_saat_externe[key] == id_ligne.num_processeur + id_ligne.num_liaison_serie:
            # Add a new line to the external SAAT window
            ajoute_ext(i, line, id_ligne, fenetre_saat_externe)
            break
    else:
        # Add a new line to the external SAAT window if no matching entry is found
        ajoute_ext(len(dictionnaire_saat_externe), line, id_ligne, fenetre_saat_externe)


def update_external_dec(id_ligne, line, dictionnaire_saat_externe, fenetre_saat_externe):
    """
    Updates the external SAAT in case of a backward step.

    Args:
        id_ligne (Identifier): Identifier of the line.
        line (str): The string of the current line.
        dictionnaire_saat_externe (dict): The external SAAT dictionary.
        fenetre_saat_externe (SAAT): The external SAAT window.
    """
    # Iterate through the external SAAT dictionary to find a matching entry
    for i, key in enumerate(dictionnaire_saat_externe, start=1):
        if dictionnaire_saat_externe[key] == id_ligne.num_processeur + id_ligne.num_liaison_serie:
            # Delete the last line in the external SAAT window
            supprime_ext(i, fenetre_saat_externe)
            break
    else:
        # Delete the last line in the external SAAT window if no matching entry is found
        supprime_ext(len(dictionnaire_saat_externe), fenetre_saat_externe)


def suivant(index
            , list_fenetre_saat
            , fenetre_saat_externe
            , archive
            , dictionnaire_saat_externe
            , list_fenetre_train
            , supression_pile):
    """
    Processes the archive line based on the index.

    Args:
        index (Index): The index object which will be incremented.
        list_fenetre_saat (list): List of SAAT windows.
        fenetre_saat_externe (SAAT): The external SAAT window.
        archive (list): Archive data.
        dictionnaire_saat_externe (dict): The external SAAT dictionary.
        list_fenetre_train (list): The list of train windows.
        supression_pile (list): The list for suppression management.
    """
    try:
        # Increment the index and format the line
        index.inc()
        ligne = ligne_format(archive[index.value])
        id_ligne = Identificateur(ligne)

        # Main SAAT processing
        # Check if the message is in return direction or not repeated
        if 'R' in id_ligne.sens_message or '0' in id_ligne.carac_repetition:
            if 'I' in id_ligne.code_message \
                    or 'S' in id_ligne.code_message \
                    or 'L' in id_ligne.code_message \
                    or 'F' in id_ligne.code_message:
                id_to_action_int(id_ligne, list_fenetre_train, list_fenetre_saat, supression_pile)
                update_ui(id_ligne, list_fenetre_saat, ligne, '#39FF14')
            elif 'M' in id_ligne.code_message:
                # Process external message
                for i, cle in enumerate(dictionnaire_saat_externe, start=1):
                    if dictionnaire_saat_externe[cle] == id_ligne.num_processeur + id_ligne.num_liaison_serie:
                        ligne = "Reçu de " + cle + " : " + id_ligne.lib_message
                update_ui(id_ligne, list_fenetre_saat, ligne, 'white')
            else:
                update_ui(id_ligne, list_fenetre_saat, ligne, 'white')
        # External SAAT processing
        else:
            update_external(id_ligne, ligne, dictionnaire_saat_externe, fenetre_saat_externe)
            update_ui(id_ligne, list_fenetre_saat, ligne, 'yellow')
    except IndexError:
        # Handle end of archive and start new archive
        if len(archive) <= index.value:
            end_menu = EndMenu()
            new_archive = end_menu.setupUi()
            # Add items from the new list
            archive.extend(new_archive)
        else:
            index.inc()
    except ValueError:
        index.inc()


def precedent(index
              , list_fenetre_saat
              , fenetre_saat_externe
              , archive
              , dictionnaire_saat_externe
              , list_fenetre_train
              , supression_pile):
    """
    Traite la ligne d'archive en fonction de l'index.

    Args:
        index (Index): L'objet index qui sera decrémenté.
        list_fenetre_saat (list): Liste des fenêtres SAAT.
        fenetre_saat_externe (SAAT): La fenêtre SAAT externe.
        archive (list): Les données d'archive.
        dictionnaire_saat_externe (dict): Le dictionnaire SAAT externe.
        list_fenetre_train (list): La liste des fenêtres de train.
    """
    if index.dec():
        # Supprime les caractères non imprimables en début de ligne et supprimer les espaces
        # en début et en fin de ligne
        ligne = ligne_format(archive[index.value + 1])
        id_ligne = Identificateur(ligne)

        # SAAT Principale
        # vérifie si le message est en sens retour ou s'il n'a pas été répété
        if 'R' in id_ligne.sens_message or '0' in id_ligne.carac_repetition:
            if 'I' in id_ligne.code_message \
                    or 'S' in id_ligne.code_message \
                    or 'L' in id_ligne.code_message \
                    or 'F' in id_ligne.code_message:
                if not id_to_action_int_dec(id_ligne, list_fenetre_train, list_fenetre_saat, supression_pile):
                    update_ui(id_ligne, list_fenetre_saat, "       Mémoire pleine, retour arrière impossible", 'red')
                    index.inc()
                else:
                    update_ui(id_ligne, list_fenetre_saat, ligne, '#39FF14')
            elif 'M' in id_ligne.code_message:
                for i, cle in enumerate(dictionnaire_saat_externe, start=1):
                    if dictionnaire_saat_externe[cle] == id_ligne.num_processeur + id_ligne.num_liaison_serie:
                        ligne = "Reçu de " + cle + " : " + id_ligne.lib_message
                update_ui(id_ligne, list_fenetre_saat, ligne, 'white')
            else:
                update_ui(id_ligne, list_fenetre_saat, ligne, 'white')
        # SAAT Exterieur
        else:
            update_external_dec(id_ligne, ligne, dictionnaire_saat_externe, fenetre_saat_externe)
            update_ui(id_ligne, list_fenetre_saat, ligne, 'yellow')


def rotate(list_fenetre_saat, windows):
    """
    Rotates the trains to be displayed in all windows.

    Args:
        list_fenetre_saat (list): List of SAAT windows.
        windows (list): List of all windows.
    """
    # Iterate through all windows to perform a rotation on the trains to be displayed
    for window in windows:
        if len(window.train_afficher) > window.nb_ligne:
            # Rotate the list of trains for the current window
            window.train_afficher = window.train_afficher[1:] + window.train_afficher[:1]
            # Update the display of the window with the rotated trains
            window.affiche_fenetre(list_fenetre_saat)



def ligne_format(archive_line):
    """
    Formats a line from the archive by removing non-printable characters and trimming whitespace.

    Args:
        archive_line (str): The line from the archive to be formatted.

    Returns:
        str: The formatted line.
    """
    # Remove non-printable characters and trim whitespace from both ends of the line
    return re.sub(r'[\x00-\x1F\x7F-\x9F\ufeff\xef\xbb\xbf]', ' ', archive_line.rstrip("\n").strip()).strip()


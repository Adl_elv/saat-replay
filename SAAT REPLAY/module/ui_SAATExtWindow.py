# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'SAATExtWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.5.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

import os
import sys
from PySide6.QtCore import (QCoreApplication, QMetaObject, QSize, Qt)
from PySide6.QtGui import (QBrush, QColor, QIcon, QPalette,QFontMetrics)
from PySide6.QtWidgets import (QFrame, QHBoxLayout, QHeaderView,
                               QLabel, QPushButton,
                               QSizePolicy, QStackedWidget, QVBoxLayout,
                               QWidget, QStyleOptionButton, QStylePainter, QStyle, QTableWidget, QTableWidgetItem)

from module.FilePath import resource_path


class Ui_MainWindow(object):
    def setupUi(self, MainWindow, CCUExt):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        chemin_svg = resource_path("icone.svg")
        MainWindow.setWindowIcon(QIcon(chemin_svg))
        MainWindow.resize(1000, MainWindow.size().height())
        MainWindow.setStyleSheet(u"background-color: rgb(45, 45, 45);")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.Top_Bar = QFrame(self.centralwidget)
        self.Top_Bar.setObjectName(u"Top_Bar")
        self.Top_Bar.setMaximumSize(QSize(16777215, 40))
        self.Top_Bar.setStyleSheet(u"background-color: rgb(35, 35, 35);")
        self.Top_Bar.setFrameShape(QFrame.NoFrame)
        self.Top_Bar.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.Top_Bar)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_toggle = QFrame(self.Top_Bar)
        self.frame_toggle.setObjectName(u"frame_toggle")
        self.frame_toggle.setMaximumSize(QSize(70, 40))
        self.frame_toggle.setStyleSheet(u"background-color: rgb(85, 170, 255);")
        self.frame_toggle.setFrameShape(QFrame.StyledPanel)
        self.frame_toggle.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_toggle)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.btn_toggle = ElidedButton(self.frame_toggle)
        self.btn_toggle.setObjectName(u"Btn_Toggle")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_toggle.sizePolicy().hasHeightForWidth())
        self.btn_toggle.setSizePolicy(sizePolicy)
        self.btn_toggle.setStyleSheet(u"color: rgb(255, 255, 255);\n"
"border: 0px solid;")
        icon = QIcon()
        chemin_svg = resource_path("list.svg")
        icon.addFile(chemin_svg, QSize(), QIcon.Normal, QIcon.Off)
        self.btn_toggle.setIcon(icon)
        self.btn_toggle.setIconSize(QSize(24, 24))

        self.verticalLayout_2.addWidget(self.btn_toggle)


        self.horizontalLayout.addWidget(self.frame_toggle)

        self.frame_top = QFrame(self.Top_Bar)
        self.frame_top.setObjectName(u"frame_top")
        self.frame_top.setFrameShape(QFrame.StyledPanel)
        self.frame_top.setFrameShadow(QFrame.Raised)
        self.verticalLayout_9 = QVBoxLayout(self.frame_top)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.label_title = QLabel(self.frame_top)
        self.label_title.setObjectName(u"label_title")
        palette = QPalette()
        brush = QBrush(QColor(255, 255, 255, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(35, 35, 35, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.label_title.setPalette(palette)

        self.verticalLayout_9.addWidget(self.label_title, 0, Qt.AlignHCenter|Qt.AlignVCenter)


        self.horizontalLayout.addWidget(self.frame_top)


        self.verticalLayout.addWidget(self.Top_Bar)

        self.Content = QFrame(self.centralwidget)
        self.Content.setObjectName(u"Content")
        self.Content.setFrameShape(QFrame.NoFrame)
        self.Content.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.Content)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_left_menu = QFrame(self.Content)
        self.frame_left_menu.setObjectName(u"frame_left_menu")
        self.frame_left_menu.setMinimumSize(QSize(70, 0))
        self.frame_left_menu.setMaximumSize(QSize(70, 16777215))
        self.frame_left_menu.setStyleSheet(u"background-color: rgb(35, 35, 35);")
        self.frame_left_menu.setFrameShape(QFrame.StyledPanel)
        self.frame_left_menu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frame_left_menu)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.frame_top_menus = QFrame(self.frame_left_menu)
        self.frame_top_menus.setObjectName(u"frame_top_menus")
        self.frame_top_menus.setFrameShape(QFrame.NoFrame)
        self.frame_top_menus.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frame_top_menus)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_3.addWidget(self.frame_top_menus, 0, Qt.AlignTop)


        self.horizontalLayout_2.addWidget(self.frame_left_menu)

        self.frame_pages = QFrame(self.Content)
        self.frame_pages.setObjectName(u"frame_pages")
        self.frame_pages.setFrameShape(QFrame.StyledPanel)
        self.frame_pages.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.frame_pages)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.stackedWidget = QStackedWidget(self.frame_pages)
        self.stackedWidget.setObjectName(u"stackedWidget")

        ############# ELEMENT 1
        btn_page_1 = ElidedButton(self.frame_top_menus)
        btn_page_1.setObjectName(u"btn_page_1")
        btn_page_1.setMinimumSize(QSize(20, 40))
        btn_page_1.setStyleSheet(u"QPushButton {\n"
"	color: rgb(255, 255, 255);\n"
"	background-color: rgb(85, 170, 255);\n"
"	border: 0px solid;\n"
"}\n")
        btn_page_1.setText(QCoreApplication.translate("MainWindow", u"Tous", None))
        page_1 = QWidget()
        page_1.setObjectName(u"page_1")
        verticalLayout_7 = QVBoxLayout(page_1)
        verticalLayout_7.setSpacing(0)
        verticalLayout_7.setObjectName(u"verticalLayout_7")
        verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        tableView = QTableWidget(page_1)
        tableView.setObjectName(u"tableView")
        tableView.setMinimumSize(QSize(0, 0))

        tableView.setFocusPolicy(Qt.NoFocus)
        tableView.setMidLineWidth(1)
        tableView.setStyleSheet(
            "color: white;"
        )

        tableView.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        tableView.horizontalHeader().setStyleSheet("""QHeaderView::section { border: 1px solid rgb(75, 160, 245);
    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                stop: 0 rgb(85, 170, 255), stop: 1 rgb(65, 150, 235)); color: white; }""")

        tableView.verticalHeader().setDefaultAlignment(Qt.AlignCenter)
        tableView.verticalHeader().setFixedWidth(50)
        tableView.verticalHeader().setMinimumSectionSize(40)
        tableView.verticalHeader().setVisible(False)

        tableView.setEditTriggers(QTableWidget.NoEditTriggers)
        tableView.setSelectionMode(QTableWidget.NoSelection)

        tableView.setRowCount(len(CCUExt))
        tableView.setColumnCount(2)

        tableView.setHorizontalHeaderLabels(["CCU", "Commande"])

        counter = 0
        for key in CCUExt:
            tableView.setItem(counter, 0, QTableWidgetItem(key))
            counter += 1
        verticalLayout_7.addWidget(tableView)
        self.stackedWidget.addWidget(page_1)
        self.verticalLayout_4.addWidget(btn_page_1)
        self.content = []
        tmp = []
        tmp.append(btn_page_1)
        tmp.append(page_1)
        tmp.append(tableView)
        self.content.append(tmp.copy())

        counter = 1
        for key in CCUExt:
            btn = ElidedButton(self.frame_top_menus)
            btn.setObjectName(u"btn_page_" + str(counter + 1))
            btn.setMinimumSize(QSize(20, 40))
            btn.setStyleSheet(u"QPushButton {\n"
                                          "	color: rgb(255, 255, 255);\n"
                                          "	background-color: rgb(35, 35, 35);\n"
                                          "	border: 0px solid;\n"
                                          "}\n"
                                          "QPushButton:hover {\n"
                                          "	background-color: rgb(85, 170, 255);\n"
                                          "}")
            btn.setText(QCoreApplication.translate("MainWindow", u"" + key, None))

            page = QWidget()
            page.setObjectName(u"page_" + str(counter + 1))
            layout = QVBoxLayout(page)
            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 0)

            table_view = QTableWidget(page_1)
            table_view.setObjectName(u"tableView_" + str(counter + 1))
            table_view.setMinimumSize(QSize(0, 0))

            table_view.setFocusPolicy(Qt.NoFocus)
            table_view.setMidLineWidth(1)
            table_view.setStyleSheet(
                "color: white;"
            )

            table_view.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
            table_view.horizontalHeader().setStyleSheet("""QHeaderView::section { border: 1px solid rgb(75, 160, 245);
    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                stop: 0 rgb(85, 170, 255), stop: 1 rgb(65, 150, 235)); color: white; }""")

            table_view.verticalHeader().setDefaultAlignment(Qt.AlignCenter)
            table_view.verticalHeader().setFixedWidth(50)
            table_view.verticalHeader().setMinimumSectionSize(40)
            table_view.verticalHeader().setVisible(False)

            table_view.setEditTriggers(QTableWidget.NoEditTriggers)
            table_view.setSelectionMode(QTableWidget.NoSelection)

            table_view.setColumnCount(2)
            table_view.setHorizontalHeaderLabels(["Commande", "Action"])

            layout.addWidget(table_view)
            self.stackedWidget.addWidget(page)
            self.verticalLayout_4.addWidget(btn)

            tmp.clear()
            tmp.append(btn)
            tmp.append(page)
            tmp.append(table_view)
            self.content.append(tmp.copy())

            counter += 1


        self.verticalLayout_5.addWidget(self.stackedWidget)


        self.horizontalLayout_2.addWidget(self.frame_pages)


        self.verticalLayout.addWidget(self.Content)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.stackedWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Saat Replay", None))
        self.btn_toggle.setText("")
        self.label_title.setText(QCoreApplication.translate("MainWindow", u"Fen\u00eatre SAAT Exterieur(s)", None))
    # retranslateUi


class ElidedButton(QPushButton):
    """
    A custom QPushButton class that displays elided (truncated) text if the text is too long to fit the button width.

    Methods:
        __init__(parent=None): Initializes a new instance of the ElidedButton class.
        paintEvent(event): Overrides the paintEvent() method to draw the button with elided text.
    """

    def __init__(self, parent=None):
        """
        Initializes a new instance of the ElidedButton class.

        Args:
            parent (QWidget): The parent widget of this button. Defaults to None.

        Returns:
            None
        """
        super().__init__(parent)

    def paintEvent(self, event):
        """
        Overrides the paintEvent() method to draw the button with elided text.

        This method calculates the available width for the text, elides the text if necessary,
        and then uses a QStylePainter to draw the button with the possibly elided text.

        Args:
            event (QPaintEvent): The paint event.

        Returns:
            None
        """
        # Create a QStyleOptionButton to use with style().drawControl()
        option = QStyleOptionButton()
        self.initStyleOption(option)

        # Calculate the available width for the text
        available_width = self.width() - 2 * self.style().pixelMetric(QStyle.PM_ButtonMargin)

        # Replace the text in the option with an elided version
        metrics = QFontMetrics(self.font())
        elidedText = metrics.elidedText(self.text(), Qt.ElideRight, available_width)
        option.text = elidedText

        # Use QStylePainter to draw the button
        painter = QStylePainter(self)
        painter.drawControl(QStyle.CE_PushButton, option)


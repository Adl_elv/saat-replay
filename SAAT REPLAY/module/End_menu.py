import sys

from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QMainWindow, QMessageBox

from module.FileManager import lecture_archive
from module.FileWindow import FileDialog
from module.FilePath import resource_path


def quit_button():
    sys.exit(0)


class EndMenu(QMainWindow):
    """
    A class representing the end-of-reading menu.

    Attributes:
        msg_box (QMessageBox): The user interface of the message box.

    Methods:
        __init__(): Initializes a new instance of EndMenu.
        setupUi(): Sets up the UI and handles user response.
        closeEvent(event): Handles the window close event.
        ok_rep(): Handles the Ok response and displays the file dialog for archive selection.
    """

    def __init__(self):
        """
        Initializes a new instance of EndMenu.

        Returns:
            None
        """
        # Creating the Alert Dialog
        super().__init__()
        self.msg_box = QMessageBox()
        chemin_svg = resource_path("icone.svg")
        self.msg_box.setWindowIcon(QIcon(chemin_svg))
        self.msg_box.setWindowTitle("Saat Replay")
        self.msg_box.setText("Voulez-vous choisir une nouvelle archive ?")

        # Change button text individually
        self.msg_box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        self.msg_box.setButtonText(QMessageBox.Ok, "Oui")
        self.msg_box.setButtonText(QMessageBox.Cancel, "Non")

        # Apply a stylesheet to center the buttons
        self.msg_box.setStyleSheet("QPushButton { text-align: center; width: 100px; }")

    def setupUi(self):
        """
        Sets up the UI and handles the user's response to the message box.

        Returns:
            The result of lecture_archive if the user chooses to open a new archive, otherwise exits the application.
        """
        # Waiting for user response
        reponse = self.msg_box.exec()

        # Checking user response
        if reponse == QMessageBox.Ok:
            return self.ok_rep()
        else:
            sys.exit(0)

    def closeEvent(self, event):
        """
        Handles the window close event by exiting the application.

        Args:
            event: The close event.

        Returns:
            None
        """
        event.accept()
        sys.exit(0)

    @staticmethod
    def ok_rep():
        """
        Handles the Ok response of the user. Opens a file dialog for archive selection and loads the selected archive.

        Returns:
            The result of lecture_archive with the selected file name.
        """
        # Create and display a dialog box to select the file
        file_archive = FileDialog()
        file_archive.show_dialog_archive()

        # Load data from archive
        return lecture_archive(file_archive.file_name)

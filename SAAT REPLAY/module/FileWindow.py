import sys

from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QFileDialog, QMainWindow
from module.FilePath import resource_path


class FileDialog(QMainWindow):
    """
    Class representing a file selection dialog window.

    Attributes:
        file_name (str): The name of the selected file.

    Methods:
        __init__(): Initializes a new instance of the FileDialog class.
        show_dialog(): Displays the file selection dialog and retrieves the name of the selected file.
    """

    def __init__(self):
        """
        Initializes a new instance of the FileDialog class. Creates a button
        which, when clicked, triggers the display of the file selection dialog.

        Returns:
            None
        """
        super().__init__()

        # Configure the main window
        self.setGeometry(300, 300, 350, 150)
        self.setWindowTitle('File Selection Window')
        chemin_svg = resource_path("icone.svg")
        self.setWindowIcon(QIcon(chemin_svg))

        self.file_name = None

    def show_dialog_archive(self):
        """
        Displays the file selection dialog for archive files. After the user selects a file,
        the full file path is stored in the file_name attribute.

        Returns:
            None
        """
        # Open the file selection dialog in the current directory
        self.file_name, _ = QFileDialog.getOpenFileName(self, "Open a file", "",
                                                        'Files (*.mam *.MAM *.txt *.TXT);;All Files (*.*)')

    def show_dialog_param(self):
        """
        Displays the directory selection dialog for setting parameters. After the user selects a directory,
        the full path of the directory is stored in the file_name attribute.

        Returns:
            None
        """
        # Open the directory selection dialog in the current directory
        self.file_name = QFileDialog.getExistingDirectory(self, 'Open a Folder', '.')

        # Exit the program if no directory is selected
        if not self.file_name:
            sys.exit(1)

    def show_dialog_select_file(self, list_widget):
        """
        Displays a file saving dialog and saves the contents of a list widget to the selected file.

        Args:
            list_widget (QListWidget): The list widget whose contents are to be saved.

        Returns:
            None
        """
        # Display a QFileDialog for saving a file
        self.file_name, _ = QFileDialog.getSaveFileName(None, "Save File", "",
                                                        'Files (*.mam *.MAM *.txt *.TXT);;All Files (*.*)')
        # Write the contents of the list widget to the selected file
        if self.file_name:
            with open(self.file_name, 'w') as f:
                for i in range(list_widget.count()):
                    f.write(list_widget.item(i).text() + '\n')


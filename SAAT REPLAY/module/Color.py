from PySide6.QtGui import QColor
from PySide6.QtCore import Qt
from collections import defaultdict
color_map = defaultdict(lambda: QColor(Qt.white))
color_map.update({
    "CROUG": QColor(Qt.red),
    "CVERT": QColor(Qt.green),
    "CBLEU": QColor(Qt.blue),
    "CPOUR": QColor(255, 0, 255),
    "CJAUN": QColor(255, 255, 0),
    "CYAN": QColor(0, 255, 255),
    "CBLAN": QColor(Qt.white),
    "CNOIR": QColor(Qt.black),
    "CIMP": QColor(255, 255, 0),
    "CPAI": QColor(Qt.green),
    "CFOR": QColor(Qt.white),
    # Add other color mappings here...
})

# Dictionary mapping color codes to corresponding QColor instances.
# Each key represents a unique color code, and the corresponding value is a QColor instance
# representing the associated color.

# Example usage:
# color_code = "CNOIR"
# color = color_map.get(color_code, QColor(Qt.white))
# print(color)
# Output: QColor(0, 0, 0) (representing the color black)

# Note: If the color code is not present in the dictionary, the default value QColor(Qt.white)
# is returned.

def insertion_ext(lib_message_args):
    """
    Generates a message for the insertion of a train into a window.

    Args:
        lib_message_args (list): List containing the window number and the train number.

    Returns:
        str: Message indicating the insertion of the train into the window.
    """
    num_fenetre = lib_message_args[0]
    num_train = lib_message_args[1]
    return f"Insertion du train {num_train} dans la fenetre {num_fenetre}"


def suppression_ext(lib_message_args):
    """
    Generates a message for the removal of a train or the last train from a window.

    Args:
        lib_message_args (list): List containing the window number and optionally the train number.

    Returns:
        str: Message indicating the removal of the train or the last train from the window.
    """
    num_fenetre = lib_message_args[0]
    if len(lib_message_args) == 1:
        return f"Suppression du dernier train dans la fenetre {num_fenetre}"
    else:
        num_train = lib_message_args[1]
        return f"Suppression du train {num_train} dans la fenetre {num_fenetre}"


def loc_ext(lib_message_args):
    """
    Generates a message for the transfer of a train from one window to another after activation of a "loc".

    Args:
        lib_message_args (list): List containing the "loc" number, the numbers of the origin and destination windows,
                                 and the train number.

    Returns:
        str: Message indicating the transfer of the train from one window to another.
    """
    num_loc = lib_message_args[0]
    num_fenetre_destination = lib_message_args[1]
    num_fenetre_origine = lib_message_args[2]
    num_train = lib_message_args[3]
    return f"Passage du train {num_train} de la fenetre {num_fenetre_origine} à la fenetre" \
           f" {num_fenetre_destination} après activation du loc {num_loc}"


def f_ext(lib_message_args):
    """
    Generates a message for either the removal of all trains from a window or the insertion of a train into a window.

    Args:
        lib_message_args (list): List optionally containing a window number and a train number.

    Returns:
        str: Message indicating either the removal of all trains from a window or the insertion of a train into a window.
    """
    if len(lib_message_args) == 2:
        num_fenetre = lib_message_args[1]
        return f"Suppression de tous les trains dans la fenetre {num_fenetre}"
    else:
        num_fenetre = lib_message_args[1]
        num_train = lib_message_args[2]
        return f"Insertion du train {num_train} dans la fenetre {num_fenetre}"


def insertion_int(lib_message_args, list_fenetre_train, list_fenetre_saat):
    """
    Inserts a train into a window and displays the window.

    Args:
        lib_message_args (list): List containing the window number and the train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.

    Returns:
        None
    """
    num_fenetre = int(lib_message_args[0])
    num_train = lib_message_args[1]
    for fenetre in list_fenetre_train:
        if fenetre.num == num_fenetre:
            fenetre.add_train(num_train)
            fenetre.affiche_fenetre(list_fenetre_saat)
            return


def insertion_int_dec(lib_message_args, list_fenetre_train, list_fenetre_saat):
    """
    Reverses the insertion of a train into a window and displays the window.

    Args:
        lib_message_args (list): List containing the window number and the train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.

    Returns:
        bool: True if the operation is successful, False otherwise.
    """
    num_fenetre = int(lib_message_args[0])
    num_train = lib_message_args[1]
    for fenetre in list_fenetre_train:
        if fenetre.num == num_fenetre:
            fenetre.del_train(num_train)
            fenetre.affiche_fenetre(list_fenetre_saat)
            break
    return True


def suppression_int(lib_message_args, list_fenetre_train, list_fenetre_saat, supression_pile):
    """
    Removes a train or the last train from a window and displays the window.

    Args:
        lib_message_args (list): List containing the window number and optionally the train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.
        suppression_pile (SuppressionPile): The stack for managing train removals.

    Returns:
        None
    """
    num_fenetre = int(lib_message_args[0])
    if len(lib_message_args) == 1:
        # Remove the last train from the specified window and update the display
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre:
                supression_pile.add(fenetre.del_train())
                fenetre.affiche_fenetre(list_fenetre_saat)
                break
    else:
        # Remove the specified train from the window and update the display
        num_train = lib_message_args[1]
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre:
                supression_pile.add(fenetre.del_train(num_train))
                fenetre.affiche_fenetre(list_fenetre_saat)
                break


def suppression_int_dec(lib_message_args, list_fenetre_train, list_fenetre_saat, supression_pile):
    """
    Reverses the removal of a train or the last train from a window and displays the window.

    Args:
        lib_message_args (list): List containing the window number and optionally the train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.
        supression_pile (SuppressionPile): The stack for managing train removals.

    Returns:
        bool: True if the operation is successful, False otherwise.
    """
    num_fenetre = int(lib_message_args[0])
    for fenetre in list_fenetre_train:
        if fenetre.num == num_fenetre:
            # Retrieve the last removed train number from the suppression pile
            num_train = supression_pile.sup()
            if num_train is not False:
                # Add the train back to the window and update the display
                if num_train:
                    fenetre.add_train(num_train)
                    fenetre.affiche_fenetre(list_fenetre_saat)
                break
            else:
                # Return False if there are no trains to reverse remove
                return False
    return True


def loc_int(lib_message_args, list_fenetre_train, list_fenetre_saat, supression_pile):
    """
    Transfers a train from one window to another after activation of a "loc" and displays the windows.

    Args:
        lib_message_args (list): List containing the "loc" number, the numbers of the origin and destination windows,
                                 and the train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.
        supression_pile (SuppressionPile): The stack for managing train removals.

    Returns:
        None
    """
    if len(lib_message_args) >= 4:
        num_fenetre_origine = int(lib_message_args[1])
        num_fenetre_destination = int(lib_message_args[2])
        num_train = lib_message_args[3]

        # Remove the train from the origin window and update the display
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre_origine:
                supression_pile.add(fenetre.del_train(num_train))
                fenetre.affiche_fenetre(list_fenetre_saat)
                break

        # Add the train to the destination window and update the display
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre_destination:
                fenetre.add_train(num_train)
                fenetre.affiche_fenetre(list_fenetre_saat)
                break


def loc_int_dec(lib_message_args, list_fenetre_train, list_fenetre_saat, supression_pile):
    """
    Reverses the transfer of a train from one window to another after activation of a "loc" and displays the windows.

    Args:
        lib_message_args (list): List containing the "loc" number, the numbers of the origin and destination windows,
                                 and the train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.
        supression_pile (SuppressionPile): The stack for managing train removals.

    Returns:
        bool: True if the operation is successful, False otherwise.
    """
    if len(lib_message_args) >= 4:
        num_fenetre_origine = int(lib_message_args[1])
        num_fenetre_destination = int(lib_message_args[2])
        num_train = lib_message_args[3]

        # Retrieve the last removed train number from the origin window using the suppression pile
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre_origine:
                num_train = supression_pile.sup()
                if num_train is not False:
                    # Add the train back to the origin window and update the display
                    if num_train:
                        fenetre.add_train(num_train)
                        fenetre.affiche_fenetre(list_fenetre_saat)
                else:
                    # Return False if there are no trains to reverse transfer
                    return False
                break

        # Remove the train from the destination window and update the display
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre_destination:
                fenetre.del_train(num_train)
                fenetre.affiche_fenetre(list_fenetre_saat)
                break
        return True
    elif len(lib_message_args) == 1:
        return True


def f_int(lib_message_args, list_fenetre_train, list_fenetre_saat, supression_pile):
    """
    Removes all trains from a window or inserts a train into a window, then displays the window.

    Args:
        lib_message_args (list): List optionally containing a window number and a train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.
        supression_pile (SuppressionPile): The stack for managing train removals.

    Returns:
        None
    """
    if len(lib_message_args) == 1:
        # Remove all trains from the specified window and update the display
        num_fenetre = int(lib_message_args[1])
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre:
                supression_pile.add(fenetre.clear_train())
                fenetre.affiche_fenetre(list_fenetre_saat)
                break
    else:
        # Insert a train into the specified window and update the display
        num_fenetre = int(lib_message_args[1])
        num_train = lib_message_args[2]
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre:
                fenetre.add_train(num_train)
                fenetre.affiche_fenetre(list_fenetre_saat)
                break


def f_int_dec(lib_message_args, list_fenetre_train, list_fenetre_saat, supression_pile):
    """
    Reverses the removal of all trains from a window or the insertion of a train into a window, then displays the window.

    Args:
        lib_message_args (list): List optionally containing a window number and a train number.
        list_fenetre_train (list): List of train windows.
        list_fenetre_saat (list): List of SAAT windows.
        supression_pile (SuppressionPile): The stack for managing train removals.

    Returns:
        bool: True if the operation is successful, False otherwise.
    """
    if len(lib_message_args) == 1:
        # Reverse the removal of all trains from the specified window and update the display
        num_fenetre = int(lib_message_args[1])
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre:
                num_train = supression_pile.sup()
                if num_train is not False:
                    # Add the trains back to the window and update the display
                    if num_train:
                        for num in num_train:
                            fenetre.add_train(num)
                        fenetre.affiche_fenetre(list_fenetre_saat)
                    break
                else:
                    # Return False if there are no trains to reverse remove
                    return False
    else:
        # Reverse the insertion of a train into the specified window and update the display
        num_fenetre = int(lib_message_args[1])
        num_train = lib_message_args[2]
        for fenetre in list_fenetre_train:
            if fenetre.num == num_fenetre:
                fenetre.del_train(num_train)
                fenetre.affiche_fenetre(list_fenetre_saat)
                break
    return True


def id_to_action_ext(id_arg):
    """
    Converts an identifier into text for the display of external SAAT actions.

    Args:
        id_arg (Identifier): The identifier to convert.

    Returns:
        str: The corresponding external action text.

    """
    # Split the message to avoid repetitions
    lib_message = id_arg.lib_message.split()

    # Handling different actions
    if 'I' in id_arg.code_message:
        return insertion_ext(lib_message)
    elif 'S' in id_arg.code_message:
        return suppression_ext(lib_message)
    elif 'L' in id_arg.code_message:
        return loc_ext(lib_message)
    elif 'F' in id_arg.code_message:
        return f_ext(lib_message)
    else:
        return "Pas d'action définie"


def id_to_action_int(id_arg, list_fenetre_train, list_fenetre_saat, suppression_pile):
    """
    Converts an identifier into an action for internal SAATs.

    Args:
        id_arg (Identifier): The identifier to convert.
        list_fenetre_train (List[Window]): The list of windows.
        list_fenetre_saat (List[SAATWindow]): The SAAT windows.
        suppression_pile (SuppressionPile): The stack for managing train removals.

    Returns:
        None
    """
    # Split the message to avoid repetitions
    lib_message = id_arg.lib_message.split()

    # Handling different actions
    if 'I' in id_arg.code_message:
        return insertion_int(lib_message, list_fenetre_train, list_fenetre_saat)
    elif 'S' in id_arg.code_message:
        return suppression_int(lib_message, list_fenetre_train, list_fenetre_saat, suppression_pile)
    elif 'L' in id_arg.code_message:
        return loc_int(lib_message, list_fenetre_train, list_fenetre_saat, suppression_pile)
    elif 'F' in id_arg.code_message:
        return f_int(lib_message, list_fenetre_train, list_fenetre_saat, suppression_pile)
    else:
        # If no action is defined for the given code message, do nothing
        return


def id_to_action_int_dec(id_arg, list_fenetre_train, list_fenetre_saat, suppression_pile):
    """
    Converts an identifier into an action for internal SAATs, specifically for undoing previous actions.

    Args:
        id_arg (Identifier): The identifier to convert.
        list_fenetre_train (List[Window]): The list of train windows.
        list_fenetre_saat (List[SAATWindow]): The SAAT windows.
        suppression_pile (SuppressionPile): The stack for managing the undo of train removals.

    Returns:
        bool: True to indicate the function was executed, even if no specific action was taken.
    """
    # Split the message to avoid repetitions
    lib_message = id_arg.lib_message.split()
    code_message = id_arg.code_message

    # Handling the reversal of different actions
    if 'I' in code_message:
        return insertion_int_dec(lib_message, list_fenetre_train, list_fenetre_saat)
    elif 'S' in code_message:
        return suppression_int_dec(lib_message, list_fenetre_train, list_fenetre_saat, suppression_pile)
    elif 'L' in code_message:
        return loc_int_dec(lib_message, list_fenetre_train, list_fenetre_saat, suppression_pile)
    elif 'F' in code_message:
        return f_int_dec(lib_message, list_fenetre_train, list_fenetre_saat, suppression_pile)

    else:
        # If no action is defined for the given code message, return True to indicate the function was executed
        return True

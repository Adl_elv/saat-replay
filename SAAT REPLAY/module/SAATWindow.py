import re
import sys

from PySide6.QtGui import QKeyEvent
from PySide6.QtWidgets import QMainWindow

from module.Affichage import defm_to_image, defb_t_to_image, defb_b_to_image
from module.ui_SAATWindow import Ui_MainWindow


class SAATWindow(QMainWindow):
    """
    Class representing a SAAT window.

    Attributes:
        ui (Ui_MainWindow): The user interface of the window.

    Methods:
        __init__(self, file, carac): Initializes a new instance of the SAATWindow class.
        closeEvent(self, event): Handles the window closing event.
        keyPressEvent(self, event): Handles the key press event.
        init_display_static(self, file, carac): Initializes the static display of the window.
    """

    def __init__(self, file, carac, index, presence_alarme):
        """
        Initializes a new instance of the SAATWindow class.

        Args:
            file (str): The content of the source file containing the information to display.
            carac: The characteristics for display.
            index (int): The index of the window.
            presence_alarme (bool): Indicates the presence of an alarm.

        Returns:
            None
        """
        self.vide = True  # True if the window is empty, False otherwise
        QMainWindow.__init__(self)  # Initialize the QMainWindow base class
        self.ui = Ui_MainWindow()  # Create a new instance of the UI
        self.ui.setupUi(self, index, presence_alarme)  # Set up the UI with the window index and alarm presence
        self.init_display_static(file, carac)  # Initialize the static display with the file content and
        # characteristics
        self.show()  # Display the window

    def closeEvent(self, event):
        """
        Handles the window closing event.

        Args:
            event: The close event.

        Returns:
            None
        """
        # Accept the close event to ensure the application closes smoothly
        event.accept()
        # Exit the application with a status code of 0 (indicating successful termination)
        sys.exit(0)

    def keyPressEvent(self, event: QKeyEvent):
        """
        Handles the key press event.

        Args:
            event (QKeyEvent): The key press event.

        Returns:
            None
        """
        # Call the base class implementation to ensure that key press events are handled appropriately
        super().keyPressEvent(event)

    def init_display_static(self, input_txt, carac):
        """
        Initializes the static display of the window.

        Args:
            input_txt: The source file containing the information to display.
            carac: The characteristics for the display.

        Returns:
            None
        """

        txt_lines = input_txt.split('\n')
        counter = 0
        pattern_start = r's.*o.*u.*r.*c.*e'
        pattern_end = r'c.*f.*i.*n.*e.*c'
        # Iterate through the file
        for line in txt_lines:
            counter += 1

            # Start of the section to read
            if re.search(pattern_start, line, re.IGNORECASE):

                # The window is not empty
                self.is_empty = False

                # Check the end of the section to read
                while not re.search(pattern_end, line, re.IGNORECASE):
                    line = txt_lines[counter]
                    words = line.strip().split()
                    # Reading the line and displaying the corresponding image
                    if words:
                        if words[0] == "DEFM":
                            counter += 1
                            line_2 = txt_lines[counter]
                            defm_to_image(self, line, line_2)
                        elif words[0] == "DEFB":
                            if words[1][1] == 'T':
                                defb_t_to_image(self, line, carac)
                            elif words[1][1] == 'G':
                                defb_b_to_image(self, line, carac)
                        counter += 1
                    else:
                        counter += 1
                break

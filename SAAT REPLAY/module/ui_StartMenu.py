# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Start_menumlGbRW.ui'
##
## Created by: Qt User Interface Compiler version 6.5.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

import os
import sys
from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QFrame, QHBoxLayout,
                               QLabel, QLineEdit, QMainWindow, QPlainTextEdit,
                               QPushButton, QSizePolicy, QVBoxLayout, QWidget, QDateTimeEdit, QStackedWidget,
                               QListWidget, QFileDialog, QCheckBox)

from module.FilePath import resource_path


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        chemin_svg = resource_path("icone.svg")
        MainWindow.setWindowIcon(QIcon(chemin_svg))
        MainWindow.resize(878, 337)
        MainWindow.setMinimumSize(QSize(0, 0))
        MainWindow.setMaximumSize(QSize(16777215, 16777215))
        palette = QPalette()
        brush = QBrush(QColor(255, 255, 255, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(45, 45, 45, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        brush2 = QBrush(QColor(67, 67, 67, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Light, brush2)
        brush3 = QBrush(QColor(56, 56, 56, 255))
        brush3.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Midlight, brush3)
        brush4 = QBrush(QColor(22, 22, 22, 255))
        brush4.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Dark, brush4)
        brush5 = QBrush(QColor(30, 30, 30, 255))
        brush5.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        palette.setBrush(QPalette.Active, QPalette.BrightText, brush)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        brush6 = QBrush(QColor(0, 0, 0, 255))
        brush6.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Shadow, brush6)
        palette.setBrush(QPalette.Active, QPalette.AlternateBase, brush4)
        brush7 = QBrush(QColor(255, 255, 220, 255))
        brush7.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.ToolTipBase, brush7)
        palette.setBrush(QPalette.Active, QPalette.ToolTipText, brush6)
        brush8 = QBrush(QColor(255, 255, 255, 127))
        brush8.setStyle(Qt.SolidPattern)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Active, QPalette.PlaceholderText, brush8)
#endif
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Light, brush2)
        palette.setBrush(QPalette.Inactive, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Inactive, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Inactive, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.BrightText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Shadow, brush6)
        palette.setBrush(QPalette.Inactive, QPalette.AlternateBase, brush4)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipBase, brush7)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipText, brush6)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Inactive, QPalette.PlaceholderText, brush8)
#endif
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Light, brush2)
        palette.setBrush(QPalette.Disabled, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.BrightText, brush)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Shadow, brush6)
        palette.setBrush(QPalette.Disabled, QPalette.AlternateBase, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipBase, brush7)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipText, brush6)
        brush9 = QBrush(QColor(22, 22, 22, 127))
        brush9.setStyle(Qt.SolidPattern)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Disabled, QPalette.PlaceholderText, brush9)
#endif
        MainWindow.setPalette(palette)
        MainWindow.setStyleSheet(u"background-color: rgb(45, 45, 45);")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        palette1 = QPalette()
        palette1.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette1.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette1.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette1.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette1.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette1.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette1.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette1.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette1.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette1.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette1.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.centralwidget.setPalette(palette1)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.Top_Bar = QFrame(self.centralwidget)
        self.Top_Bar.setObjectName(u"Top_Bar")
        self.Top_Bar.setMaximumSize(QSize(16777215, 40))
        self.Top_Bar.setStyleSheet(u"background-color: rgb(35, 35, 35);")
        self.Top_Bar.setFrameShape(QFrame.NoFrame)
        self.Top_Bar.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.Top_Bar)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.Label_Title = QFrame(self.Top_Bar)
        self.Label_Title.setObjectName(u"Label_Title")
        self.Label_Title.setFrameShape(QFrame.StyledPanel)
        self.Label_Title.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_6 = QHBoxLayout(self.Label_Title)
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.label_1 = QLabel(self.Label_Title)
        self.label_1.setObjectName(u"label_1")
        palette2 = QPalette()
        palette2.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush10 = QBrush(QColor(35, 35, 35, 255))
        brush10.setStyle(Qt.SolidPattern)
        palette2.setBrush(QPalette.Active, QPalette.Button, brush10)
        palette2.setBrush(QPalette.Active, QPalette.Base, brush10)
        palette2.setBrush(QPalette.Active, QPalette.Window, brush10)
        palette2.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette2.setBrush(QPalette.Inactive, QPalette.Button, brush10)
        palette2.setBrush(QPalette.Inactive, QPalette.Base, brush10)
        palette2.setBrush(QPalette.Inactive, QPalette.Window, brush10)
        palette2.setBrush(QPalette.Disabled, QPalette.Button, brush10)
        palette2.setBrush(QPalette.Disabled, QPalette.Base, brush10)
        palette2.setBrush(QPalette.Disabled, QPalette.Window, brush10)
        self.label_1.setPalette(palette2)

        self.horizontalLayout_6.addWidget(self.label_1, 0, Qt.AlignHCenter | Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.Label_Title)

        self.verticalLayout.addWidget(self.Top_Bar)

        self.content = QFrame(self.centralwidget)
        self.content.setObjectName(u"content")
        self.content.setFrameShape(QFrame.StyledPanel)
        self.content.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.content)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.partie_gauche = QFrame(self.content)
        self.partie_gauche.setObjectName(u"partie_gauche")
        self.partie_gauche.setMinimumSize(QSize(400, 0))
        self.partie_gauche.setFrameShape(QFrame.StyledPanel)
        self.partie_gauche.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.partie_gauche)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.description = QFrame(self.partie_gauche)
        self.description.setObjectName(u"description")
        self.description.setMaximumSize(QSize(16777215, 39))
        self.description.setFrameShape(QFrame.StyledPanel)
        self.description.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QHBoxLayout(self.description)
        self.verticalLayout_5.setSpacing(10)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(10, 10, 10, 10)
        self.label = QLabel(self.description)
        self.label.setObjectName(u"label")
        palette3 = QPalette()
        palette3.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette3.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette3.setBrush(QPalette.Active, QPalette.Text, brush)
        palette3.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette3.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette3.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette3.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette3.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette3.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette3.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette3.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette3.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette3.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.label.setPalette(palette3)

        self.verticalLayout_5.addWidget(self.label)

        self.dialog_selection = MyLineEdit()
        self.verticalLayout_5.addWidget(self.dialog_selection)

        self.verticalLayout_2.addWidget(self.description)

        self.formulaire = QFrame(self.partie_gauche)
        self.formulaire.setObjectName(u"formulaire")
        self.formulaire.setFrameShape(QFrame.StyledPanel)
        self.formulaire.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.formulaire)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.information = QFrame(self.formulaire)
        self.information.setObjectName(u"information")
        self.information.setFrameShape(QFrame.StyledPanel)
        self.information.setFrameShadow(QFrame.Raised)
        self.verticalLayout_6 = QVBoxLayout(self.information)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.information1 = QFrame(self.information)
        self.information1.setObjectName(u"information1")
        self.information1.setMaximumSize(QSize(16777215, 47))
        self.information1.setFrameShape(QFrame.StyledPanel)
        self.information1.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.information1)
        self.horizontalLayout_4.setSpacing(10)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(10, 10, 10, 10)
        self.choix_index = 0
        self.choix_menu = QComboBox(self.information1)
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.addItem("")
        self.choix_menu.setObjectName(u"choix_menu")
        self.choix_menu.setMinimumSize(QSize(0, 0))
        self.choix_menu.setStyleSheet("""
        QComboBox QAbstractItemView {
            color: darkgray; 
        }
        """)
        self.choix_menu.currentIndexChanged.connect(self.on_combo_changed)

        palette4 = QPalette()
        palette4.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette4.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette4.setBrush(QPalette.Active, QPalette.Text, brush)
        palette4.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette4.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette4.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette4.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette4.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette4.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette4.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette4.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette4.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette4.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette4.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette4.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.choix_menu.setPalette(palette4)

        self.horizontalLayout_4.addWidget(self.choix_menu)

        self.choix_saisie = QStackedWidget(self.information)

        self.text = QLineEdit(self.information)
        self.text.setObjectName(u"text")
        self.text.setStyleSheet("color: white;")

        self.date = QDateTimeEdit(self.information)
        self.date.setObjectName(u"date")
        self.date.setStyleSheet("color: white;")

        self.alarmSelectionWidget = QWidget(self.information)
        self.alarmSelectionLayout = QHBoxLayout(self.alarmSelectionWidget)
        self.labelSelectAlarms = QLabel("Sélectionner les alarmes")
        self.labelSelectAlarms.setObjectName(u"alarm label")
        self.labelSelectAlarms.setStyleSheet("color: white;")
        self.checkbox = QCheckBox(self.information1)
        self.checkbox.setObjectName(u"checkbox")
        self.checkbox.setStyleSheet("color: white;")
        self.alarmSelectionLayout.addWidget(self.labelSelectAlarms)
        self.alarmSelectionLayout.addWidget(self.checkbox)
        self.alarmSelectionLayout.setContentsMargins(0, 0, 0, 0)

        self.choix_saisie.addWidget(self.text)  # index 0
        self.choix_saisie.addWidget(self.date)  # index 1
        self.choix_saisie.addWidget(self.alarmSelectionWidget)  # index 2

        self.horizontalLayout_4.addWidget(self.choix_saisie)

        self.verticalLayout_6.addWidget(self.information1)


        self.verticalLayout_4.addWidget(self.information)

        self.ajout = QFrame(self.formulaire)
        self.ajout.setObjectName(u"ajout")
        self.ajout.setFrameShape(QFrame.StyledPanel)
        self.ajout.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.ajout)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.et = QPushButton(self.ajout)
        self.et.setObjectName(u"et")
        self.et.setMinimumSize(QSize(0, 40))
        palette5 = QPalette()
        palette5.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette5.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette5.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette5.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette5.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette5.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette5.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette5.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette5.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette5.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette5.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.et.setPalette(palette5)
        font = QFont()
        font.setFamilies([u"Sans Serif"])
        self.et.setFont(font)

        self.horizontalLayout_5.addWidget(self.et)

        self.ou = QPushButton(self.ajout)
        self.ou.setObjectName(u"ou")
        self.ou.setMinimumSize(QSize(0, 40))
        palette6 = QPalette()
        palette6.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette6.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette6.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette6.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette6.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette6.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette6.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette6.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette6.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette6.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette6.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.ou.setPalette(palette6)
        self.ou.setFont(font)

        self.horizontalLayout_5.addWidget(self.ou)


        self.verticalLayout_4.addWidget(self.ajout)


        self.verticalLayout_2.addWidget(self.formulaire)

        self.boutton = QFrame(self.partie_gauche)
        self.boutton.setObjectName(u"boutton")
        self.boutton.setMaximumSize(QSize(16777215, 50))
        self.boutton.setFrameShape(QFrame.StyledPanel)
        self.boutton.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.boutton)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.pushButton_3 = QPushButton(self.boutton)
        self.pushButton_3.setObjectName(u"pushButton_3")
        icon1 = QIcon()
        chemin_svg = resource_path("save.svg")
        icon1.addFile(chemin_svg, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_3.setIcon(icon1)
        self.pushButton_3.setIconSize(QSize(40, 40))

        self.horizontalLayout_3.addWidget(self.pushButton_3)

        self.pushButton_1 = QPushButton(self.boutton)
        self.pushButton_1.setObjectName(u"pushButton_1")
        self.pushButton_1.setMaximumSize(QSize(16777215, 16777215))
        icon2 = QIcon()
        chemin_svg = resource_path("search.svg")
        icon2.addFile(chemin_svg, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_1.setIcon(icon2)
        self.pushButton_1.setIconSize(QSize(40, 40))

        self.horizontalLayout_3.addWidget(self.pushButton_1)

        self.pushButton_2 = QPushButton(self.boutton)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setMaximumSize(QSize(16777215, 16777215))
        icon3 = QIcon()
        chemin_svg = resource_path("check.svg")
        icon3.addFile(chemin_svg, QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_2.setIcon(icon3)
        self.pushButton_2.setIconSize(QSize(40, 40))

        self.horizontalLayout_3.addWidget(self.pushButton_2)


        self.verticalLayout_2.addWidget(self.boutton)


        self.horizontalLayout_2.addWidget(self.partie_gauche)

        self.archive = QFrame(self.content)
        self.archive.setObjectName(u"archive")
        self.archive.setMinimumSize(QSize(476, 0))
        self.archive.setFrameShape(QFrame.StyledPanel)
        self.archive.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.archive)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.list_widget = QListWidget(self.archive)
        self.list_widget.setObjectName(u"list_widget")
        self.list_widget.setStyleSheet("""
            QListWidget {
                background-color: #2D2D2D;
                color: white;
            }
            QListWidget::item:selected {
                background-color: blue;
            }
        """)
        palette7 = QPalette()
        palette7.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette7.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette7.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette7.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette7.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette7.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette7.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette7.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette7.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.list_widget.setPalette(palette7)

        self.verticalLayout_3.addWidget(self.list_widget)


        self.horizontalLayout_2.addWidget(self.archive)


        self.verticalLayout.addWidget(self.content)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Saat Replay", None))
        self.label_1.setText(QCoreApplication.translate("MainWindow", u"Menu", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Choisissez votre archive :", None))
        self.choix_menu.setItemText(0, QCoreApplication.translate("MainWindow", u"Tous", None))
        self.choix_menu.setItemText(1, QCoreApplication.translate("MainWindow", u"\u00c0 partir de", None))
        self.choix_menu.setItemText(2, QCoreApplication.translate("MainWindow", u"Jusqu'a", None))
        self.choix_menu.setItemText(3, QCoreApplication.translate("MainWindow", u"Alarme", None))
        self.choix_menu.setItemText(4, QCoreApplication.translate("MainWindow", u"Code alarme", None))
        self.choix_menu.setItemText(5, QCoreApplication.translate("MainWindow", u"Sens du message", None))
        self.choix_menu.setItemText(6, QCoreApplication.translate("MainWindow", u"Num\u00e9ro de liaison s\u00e9rie", None))
        self.choix_menu.setItemText(7, QCoreApplication.translate("MainWindow", u"Caract\u00e8re de r\u00e9p\u00e9tition", None))
        self.choix_menu.setItemText(8, QCoreApplication.translate("MainWindow", u"Type du message", None))
        self.choix_menu.setItemText(9, QCoreApplication.translate("MainWindow", u"Information du message", None))

        self.et.setText(QCoreApplication.translate("MainWindow", u"ET", None))
        self.ou.setText(QCoreApplication.translate("MainWindow", u"OU", None))
        self.pushButton_3.setText("")
        self.pushButton_1.setText("")
        self.pushButton_2.setText("")
    # retranslateUi

    def on_combo_changed(self, index):

        self.choix_index = index
        if index == 1 or index == 2:
            self.choix_saisie.setCurrentIndex(1)  # show QDateTimeEdit
        elif index == 3:
            self.choix_saisie.setCurrentIndex(2)  # show QCheckBox
        else:
            self.choix_saisie.setCurrentIndex(0)  # show QLineEdit


class MyLineEdit(QLineEdit):
    """
    A customized QLineEdit widget that opens a file dialog on mouse click and sets the selected file's path as its text.

    The widget is set to read-only mode, preventing manual text entry but allowing file path selection through a file dialog.
    """

    def __init__(self, start_menu=None, *args, **kwargs):
        """
        Initializes a new instance of the MyLineEdit class, setting it to read-only and applying a text color style.

        Args:
            *args: Variable length argument list.
            **kwargs: Arbitrary keyword arguments.
        """
        super(MyLineEdit, self).__init__(*args, **kwargs)
        self.setReadOnly(True)  # Set the QLineEdit to read-only
        self.setStyleSheet("color: white;")  # Set the text color to white
        self.start_menu = start_menu

    def set_menu(self, start_menu):
        self.start_menu = start_menu

    def mousePressEvent(self, event):
        """
        Overrides the mousePressEvent to open a file dialog when the widget is clicked.

        If a file is selected, its path is set as the text of the QLineEdit widget.

        Args:
            event (QMouseEvent): The mouse press event.
        """
        # Open a file dialog when the widget is clicked
        filename, _ = QFileDialog.getOpenFileName(None, "Choose an archive", "", 'Files (*.mam *.MAM *.txt *.TXT);;All Files (*.*)')
        if filename:
            # Set the selected file's path as the widget's text
            self.setText(filename)
            if self.start_menu:
                self.start_menu.search_event()

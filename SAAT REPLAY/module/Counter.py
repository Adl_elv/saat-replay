class Counter:
    """
    A simple counter class.

    Attributes:
        value (int): The value of the counter.

    Methods:
        __init__(): Initializes a new Counter object with an initial value of -1.
        inc(): Increments the counter value by 1.
        dec(): Decrements the counter value by 1 and returns a boolean indicating if the decrement was successful.
    """
    def __init__(self):
        """
        Initializes a Counter object with an initial value of -1.

        Returns:
            None
        """
        self.value = -1

    def inc(self):
        """
        Increments the counter value by 1.

        Returns:
            None
        """
        self.value += 1

    def dec(self):
        """
        Decrements the counter value by 1.

        Returns:
            bool: True if the counter was decremented successfully, False if the counter value was already 0 or less.
        """
        if self.value > 0:
            self.value -= 1
            return True
        else:
            return False

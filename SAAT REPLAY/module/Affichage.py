import re

from PySide6.QtGui import QColor, QBrush, QFont, QPen

from module.Color import color_map


def draw_text_in_boxes(text, saat_window, color_back, color_front, pos_x, pos_y):
    """
    Draws text at the specified position (pos_x, pos_y) in the given SAAT window using background and foreground colors.

    Arguments:
        text (str): The text to be drawn.
        saat_window (SAATWindow): The graphical window in which to draw.
        color_back (str or QColor): The background color for the boxes. Can be either a string representing
                                    an HTML color or a QColor instance.
        color_front (str or QColor): The text color. Can be either a string representing an HTML color
                                     or a QColor instance.
        pos_x (int): The starting horizontal position for the boxes.
        pos_y (int): The starting vertical position for the boxes.

    Returns:
        None
    """

    # Set the font for the text
    text_font = QFont("Courier", 12)

    # Dimensions of the box
    box_width = 16
    box_height = 21

    # Initial position
    x = 0
    y = 0

    # Color for the box
    pen_back = QPen()
    pen_back.setWidth(0)
    pen_back.setColor(color_back)

    # Draw each character in a box
    for char in text:

        # Draw the box
        saat_window.ui.scene.addRect(x + pos_x * 16, y + pos_y * 22, box_width, box_height, pen_back,
                                     QBrush(color_back))

        # Draw the character
        text_item = saat_window.ui.scene.addText(char, text_font)
        text_item.setPos(x + (box_width - text_item.boundingRect().width()) / 2 + pos_x * 16,
                         y + (box_height - text_item.boundingRect().height()) / 2 + pos_y * 22)
        text_item.setDefaultTextColor(QColor(color_front))

        # Update the x position for the next box
        x += box_width


def hexa_to_pixel(hex_value, saat_window, color_background, color_txt, pos_x, pos_y):
    """
    Converts a hexadecimal value to pixels and draws them in boxes on the specified graphical window.

    Arguments:
        hex_value (str): The hexadecimal value to convert into pixels.
        saat_window (SAATWindow): The graphical window in which to draw.
        color_background (str or QColor): The background color for the pixels. Can be either a string representing
                                          an HTML color or a QColor instance.
        color_txt (str or QColor): The color of the pixels. Can be either a string representing an HTML color
                                   or a QColor instance.
        pos_x (int): The starting horizontal position for the pixels.
        pos_y (int): The starting vertical position for the pixels.

    Returns:
        None
    """

    # Convert the hexadecimal value to 8-bit binary
    binary_values = [format(int(h[:-1], 16), '08b') for h in hex_value]
    binary_values = binary_values[:11]  # Limit the number of lines to 11

    # Reverse the values for correct display orientation
    binary_values = [b[::-1] for b in binary_values]

    # Space assigned to 1 pixel
    pixel_size = 1

    # Color for the image
    pen_front = QPen()
    pen_front.setWidth(0)
    pen_front.setColor(color_txt)

    # Background color
    pen_back = QPen()
    pen_back.setWidth(0)
    pen_back.setColor(color_background)

    # Draw the pixels
    for i, row in enumerate(binary_values):
        for j, pixel in enumerate(row):

            # Draw the background
            saat_window.ui.scene.addRect(j * pixel_size * 2 + pos_x * 16, i * pixel_size * 2 + pos_y * 22, pixel_size,
                                         pixel_size, pen_back, QBrush(color_background))

            # Draw a pixel if it is present
            if pixel != '0':
                saat_window.ui.scene.addRect(j * pixel_size * 2 + pos_x * 16, i * pixel_size * 2 + pos_y * 22,
                                             pixel_size, pixel_size, pen_front, QBrush(color_txt))


def defm_to_image(saat_window, line_1, line_2):
    """
    Converts a text definition into an image and displays it in the SAAT window.

    Arguments:
        saat_window (SAATWindow): The SAAT window where the image will be displayed.
        line_1 (str): The text definition line.
        line_2 (str): The text location line.

    Returns:
        None
    """
    pattern_defm = r"T(.*?)#"
    match = re.search(pattern_defm, line_1)

    if match:
        extracted_text = match.group(1)  # Extracts the text between "T" and "#"

        # Extract colors
        colors = line_2.split(",")[0].split("DEFB ")[1].split("+")
        background = colors[0][:-1].strip()
        foreground = colors[1][:-1].strip()

        # Convert colors to Qt.Color
        background_color = color_map.get(background.upper())
        foreground_color = color_map.get(foreground.upper())

        # Extract coordinates
        coords = line_2.split(",")[1:]
        x_coord = int(coords[1].strip())
        y_coord = int(coords[0].strip())

        # Display
        draw_text_in_boxes(extracted_text, saat_window, background_color, foreground_color, x_coord - 1, y_coord - 1)


def defb_t_to_image(saat_window, line, carac):
    """
    Converts a character definition into an image and displays it in the SAAT window.
    T is fo character alone

    Arguments:
        saat_window (SAATWindow): The SAAT window where the image will be displayed.
        line (str): The line defining the text block.
        carac (str): The characters for display.

    Returns:
        None
    """

    # Extract colors
    colors = line.split(",")[3].split("+")
    background = colors[0][:-1].strip()
    foreground = colors[1][:-1].strip()

    # Convert color names to color values (you can use Qt.Color here)
    background_color = color_map.get(background.upper())
    foreground_color = color_map.get(foreground.upper())

    # Extract coordinates
    coords = line.split(",")[4:]
    x_coord = int(coords[1].strip())
    y_coord = int(coords[0].strip())

    # Check if the object to display is a character (C) or plain text
    if line.split(",")[1][0] != 'C':
        # Extract the plain text
        text = line.split(",")[1].replace("'", "").strip()
        # Display
        draw_text_in_boxes(text, saat_window, background_color, foreground_color, x_coord - 1, y_coord - 1)

    else:
        # Extract the character number after "C"
        c_number = line.split(",")[1].replace("C", "").strip()
        # Display
        hexa_to_pixel(carac[int(c_number)], saat_window, background_color, foreground_color, x_coord - 1, y_coord - 1)


def defb_b_to_image(saat_window, line, carac):
    """
    Converts a character definition into multiple images and displays them in the SAAT window.
    B is for G character meaning drawing multiple time the same character
    Arguments:
        saat_window (SAATWindow): The SAAT window where the images will be displayed.
        line (str): The line defining the text block.
        carac (str): The characters for display.

    Returns:
        None
    """

    # Extract colors
    colors = line.split(",")[2].split("+")
    background = colors[0][:-1].strip()
    foreground = colors[1][:-1].strip()

    # Convert color names to color values (you can use Qt.Color here)
    background_color = color_map.get(background.upper())
    foreground_color = color_map.get(foreground.upper())

    # Extract coordinates
    coords1 = [int(coord.strip()) for coord in line.split(",")[3:5]]
    coords2 = [int(coord.strip()) for coord in line.split(",")[5:7]]

    # Check if the object to display is a character (C) or plain text
    if line.split(",")[1][0] != 'C':
        # Extract the plain text
        text = line.split(",")[1].replace("'", "").strip()
        # Display
        for y in range(coords1[0], coords2[0] + 1):
            for x in range(coords1[1], coords2[1] + 1):
                draw_text_in_boxes(text, saat_window, background_color, foreground_color, x - 1, y - 1)

    else:
        # Extract the character number after "C"
        c_number = line.split(",")[1].replace("C", "").strip()
        # Display
        for y in range(coords1[0], coords2[0] + 1):
            for x in range(coords1[1], coords2[1] + 1):
                hexa_to_pixel(carac[int(c_number)], saat_window, background_color, foreground_color, x - 1, y - 1)

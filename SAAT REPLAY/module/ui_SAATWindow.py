# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'SAATWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.5.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QMetaObject, QSize, Qt)
from PySide6.QtGui import (QBrush, QColor, QPalette, QIcon)
from PySide6.QtWidgets import (QFrame, QGraphicsView, QHBoxLayout, QLabel, QVBoxLayout, QWidget, QGraphicsScene)
from module.FilePath import resource_path

class Ui_MainWindow(object):
    def setupUi(self, MainWindow, index, presence_alarme):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        if presence_alarme == 1:
            MainWindow.resize(1310, 1000)
        else:
            MainWindow.resize(1310, 1157)
        chemin_svg = resource_path("icone.svg")
        MainWindow.setWindowIcon(QIcon(chemin_svg))
        MainWindow.setStyleSheet(u"background-color: black;")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        palette = QPalette()
        brush = QBrush(QColor(255, 255, 255, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(45, 45, 45, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        self.centralwidget.setPalette(palette)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.Top_Bar = QFrame(self.centralwidget)
        self.Top_Bar.setObjectName(u"Top_Bar")
        self.Top_Bar.setMaximumSize(QSize(16777215, 40))
        self.Top_Bar.setStyleSheet(u"background-color: rgb(35, 35, 35);")
        self.Top_Bar.setFrameShape(QFrame.NoFrame)
        self.Top_Bar.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.Top_Bar)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.Label_Title = QFrame(self.Top_Bar)
        self.Label_Title.setObjectName(u"Label_Title")
        self.Label_Title.setFrameShape(QFrame.StyledPanel)
        self.Label_Title.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_6 = QHBoxLayout(self.Label_Title)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label_1 = QLabel(self.Label_Title)
        self.label_1.setObjectName(u"label_1")
        palette1 = QPalette()
        palette1.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush2 = QBrush(QColor(35, 35, 35, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette1.setBrush(QPalette.Active, QPalette.Button, brush2)
        palette1.setBrush(QPalette.Active, QPalette.Base, brush2)
        palette1.setBrush(QPalette.Active, QPalette.Window, brush2)
        palette1.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette1.setBrush(QPalette.Inactive, QPalette.Button, brush2)
        palette1.setBrush(QPalette.Inactive, QPalette.Base, brush2)
        palette1.setBrush(QPalette.Inactive, QPalette.Window, brush2)
        palette1.setBrush(QPalette.Disabled, QPalette.Button, brush2)
        palette1.setBrush(QPalette.Disabled, QPalette.Base, brush2)
        palette1.setBrush(QPalette.Disabled, QPalette.Window, brush2)
        self.label_1.setPalette(palette1)

        self.horizontalLayout_6.addWidget(self.label_1, 0, Qt.AlignHCenter|Qt.AlignVCenter)


        self.horizontalLayout.addWidget(self.Label_Title)


        self.verticalLayout.addWidget(self.Top_Bar)

        self.Content = QFrame(self.centralwidget)
        self.Content.setObjectName(u"Content")
        self.Content.setFrameShape(QFrame.NoFrame)
        self.Content.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.Content)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.View = QFrame(self.Content)
        self.View.setObjectName(u"View")
        self.View.setFrameShape(QFrame.StyledPanel)
        self.View.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_7 = QHBoxLayout(self.View)
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.scene = QGraphicsScene(self.View)

        if presence_alarme == 1:
            self.scene.setSceneRect(0, 0, 1280, 826)
        else:
            self.scene.setSceneRect(0, 0, 1280, 1076)

        self.graphicsView = QGraphicsView(self.scene)
        self.graphicsView.setObjectName(u"graphicsView")

        self.horizontalLayout_7.addWidget(self.graphicsView)


        self.horizontalLayout_2.addWidget(self.View)


        self.verticalLayout.addWidget(self.Content)

        self.Bot_Bar = QFrame(self.centralwidget)
        self.Bot_Bar.setObjectName(u"Bot_Bar")
        self.Bot_Bar.setMinimumSize(QSize(0, 30))
        self.Bot_Bar.setMaximumSize(QSize(16777215, 30))
        self.Bot_Bar.setStyleSheet(u"background-color: rgb(35, 35, 35);")
        self.Bot_Bar.setFrameShape(QFrame.StyledPanel)
        self.Bot_Bar.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.Bot_Bar)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.Time = QFrame(self.Bot_Bar)
        self.Time.setObjectName(u"Time")
        self.Time.setMaximumSize(QSize(220, 16777215))
        self.Time.setFrameShape(QFrame.StyledPanel)
        self.Time.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.Time)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.label_2 = QLabel(self.Time)
        self.label_2.setObjectName(u"label_2")
        palette2 = QPalette()
        palette2.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette2.setBrush(QPalette.Active, QPalette.Button, brush2)
        palette2.setBrush(QPalette.Active, QPalette.Base, brush2)
        palette2.setBrush(QPalette.Active, QPalette.Window, brush2)
        palette2.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette2.setBrush(QPalette.Inactive, QPalette.Button, brush2)
        palette2.setBrush(QPalette.Inactive, QPalette.Base, brush2)
        palette2.setBrush(QPalette.Inactive, QPalette.Window, brush2)
        palette2.setBrush(QPalette.Disabled, QPalette.Button, brush2)
        palette2.setBrush(QPalette.Disabled, QPalette.Base, brush2)
        palette2.setBrush(QPalette.Disabled, QPalette.Window, brush2)
        self.label_2.setPalette(palette2)

        self.horizontalLayout_5.addWidget(self.label_2)


        self.horizontalLayout_3.addWidget(self.Time)

        self.Alarm = QFrame(self.Bot_Bar)
        self.Alarm.setObjectName(u"Alarm")
        self.Alarm.setFrameShape(QFrame.StyledPanel)
        self.Alarm.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.Alarm)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.label_3 = QLabel(self.Alarm)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMaximumSize(QSize(115, 16777215))
        palette3 = QPalette()
        palette3.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette3.setBrush(QPalette.Active, QPalette.Button, brush2)
        palette3.setBrush(QPalette.Active, QPalette.Base, brush2)
        palette3.setBrush(QPalette.Active, QPalette.Window, brush2)
        palette3.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette3.setBrush(QPalette.Inactive, QPalette.Button, brush2)
        palette3.setBrush(QPalette.Inactive, QPalette.Base, brush2)
        palette3.setBrush(QPalette.Inactive, QPalette.Window, brush2)
        palette3.setBrush(QPalette.Disabled, QPalette.Button, brush2)
        palette3.setBrush(QPalette.Disabled, QPalette.Base, brush2)
        palette3.setBrush(QPalette.Disabled, QPalette.Window, brush2)
        self.label_3.setPalette(palette3)

        self.horizontalLayout_4.addWidget(self.label_3)

        self.label_4 = QLabel(self.Alarm)
        self.label_4.setObjectName(u"label_4")
        palette4 = QPalette()
        palette4.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette4.setBrush(QPalette.Active, QPalette.Button, brush2)
        palette4.setBrush(QPalette.Active, QPalette.Base, brush2)
        palette4.setBrush(QPalette.Active, QPalette.Window, brush2)
        palette4.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette4.setBrush(QPalette.Inactive, QPalette.Button, brush2)
        palette4.setBrush(QPalette.Inactive, QPalette.Base, brush2)
        palette4.setBrush(QPalette.Inactive, QPalette.Window, brush2)
        palette4.setBrush(QPalette.Disabled, QPalette.Button, brush2)
        palette4.setBrush(QPalette.Disabled, QPalette.Base, brush2)
        palette4.setBrush(QPalette.Disabled, QPalette.Window, brush2)
        self.label_4.setPalette(palette4)
        self.label_4.setStyleSheet("color: red;")

        self.horizontalLayout_4.addWidget(self.label_4)

        self.horizontalLayout_3.addWidget(self.Alarm)

        self.Archive = QFrame(self.Bot_Bar)
        self.Archive.setObjectName(u"Archive")
        self.Archive.setFrameShape(QFrame.StyledPanel)
        self.Archive.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_8 = QHBoxLayout(self.Archive)
        self.horizontalLayout_8.setSpacing(0)
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.label_5 = QLabel(self.Archive)
        self.label_5.setObjectName(u"label_5")
        palette5 = QPalette()
        palette5.setBrush(QPalette.Active, QPalette.WindowText, brush)
        palette5.setBrush(QPalette.Active, QPalette.Button, brush2)
        palette5.setBrush(QPalette.Active, QPalette.Base, brush2)
        palette5.setBrush(QPalette.Active, QPalette.Window, brush2)
        palette5.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette5.setBrush(QPalette.Inactive, QPalette.Button, brush2)
        palette5.setBrush(QPalette.Inactive, QPalette.Base, brush2)
        palette5.setBrush(QPalette.Inactive, QPalette.Window, brush2)
        palette5.setBrush(QPalette.Disabled, QPalette.Button, brush2)
        palette5.setBrush(QPalette.Disabled, QPalette.Base, brush2)
        palette5.setBrush(QPalette.Disabled, QPalette.Window, brush2)
        self.label_5.setPalette(palette5)

        self.horizontalLayout_8.addWidget(self.label_5)

        self.horizontalLayout_3.addWidget(self.Archive)

        self.verticalLayout.addWidget(self.Bot_Bar)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow, index)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow, index):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Saat Replay", None))
        self.label_1.setText(QCoreApplication.translate("MainWindow", u"SAAT Fen\u00eatre " + index, None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.label_2.setStyleSheet("font-size: 17px;")
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Alarme  : ", None))
        self.label_3.setStyleSheet("font-size: 17px;")
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"", None))
        self.label_4.setStyleSheet("color: red; font-size: 17px;")
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"", None))
        # retranslateUi


from module.Affichage import draw_text_in_boxes
from PySide6.QtCore import Qt


# Constant for the colors
COULEUR_BACK = Qt.black  # Color of the window background
COULEUR_VDB = Qt.white  # Color of the overload indicator


class Fenetre:
    """
    A class representing a window in the application's interface.

    Attributes:
        num (int): The number of the window.
        ecran (int): The screen number where the window is displayed.
        nb_ligne (int): The number of lines that can be displayed in the window.
        nb_ligne_max (int): The maximum number of lines for the window.
        pos (list): The position of the window.
        vdb (int): The position of the overflow indicator for the window.
        index_pair (Qt.green): Color for even indices.
        index_impair (Qt.green): Color for odd indices.
        index_indetermine (Qt.green): Color for indeterminate indices.
        train_afficher (list): List of trains to display.
        train_stocker (list): List of stored trains.

    Methods:
        __str__(): Returns a string representation of the window.
        add_train(index): Adds a train to the window.
        del_train(index): Removes a train from the window.
        clear_train(): Removes all trains from the window.
        affiche_train(fenetre_saat, train, pos_x, pos_y): Displays the trains in the window.
        affiche_fenetre(list_fenetre_saat): Displays the window.
        affiche_fenetre_visible(list_fenetre_saat): Displays the visible window.
        affiche_fenetre_invisible(list_fenetre_saat): Displays the invisible window.
        ini_fenetre_invisible(list_fenetre_saat): Initializes the invisible window.
    """
    def __init__(self):
        """
        Initializes a new window with default values.

        Returns:
            None
        """
        self.num = None
        self.ecran = None
        self.nb_ligne = None
        self.nb_ligne_max = None
        self.pos = None
        self.vdb = None
        self.index_pair = Qt.green
        self.index_impair = Qt.green
        self.index_indetermine = Qt.green
        self.train_afficher = []
        self.train_stocker = []

    def __str__(self):
        """
        Returns a string representation of the window.

        Returns:
            str: The representation of the window.
        """
        return (f"Fenetre:\n"
                f"\tNumero: {self.num}\n"
                f"\tEcran: {self.ecran}\n"
                f"\tNombre de lignes visible: {self.nb_ligne}\n"
                f"\tNombre de lignes maximum: {self.nb_ligne_max}\n"
                f"\tPositions: {self.pos}\n"
                f"\tVDB: {self.vdb}\n"
                f"\tTrain: {self.train_stocker}")

    def add_train(self, index):
        """
        Adds a train to the window.

        Args:
            index (int): The index of the train to be added.

        Returns:
            None
        """
        if self.vdb is None:
            if len(self.train_stocker) < self.nb_ligne:
                self.train_stocker.append(index)
                self.train_afficher.append(index)
        else:
            if len(self.train_stocker) < self.nb_ligne_max:
                self.train_stocker.append(index)
                self.train_afficher.append(index)

    def del_train(self, index=None):
        """
        Removes a train from the window.

        Args:
            index (int, optional): The index of the train to be removed. If not provided, the last train is removed.

        Returns:
            int or None: The index of the removed train if successful, None otherwise.
        """
        if index is None:
            if len(self.train_stocker) > 0:
                index = self.train_stocker[-1]

        if index in self.train_stocker:
            self.train_stocker.remove(index)
            self.train_afficher.remove(index)
            return index
        return None

    def clear_train(self):
        """
        Removes all trains from the window.

        Returns:
            list: A list of the indices of the trains that were removed.
        """
        trains = self.train_stocker.copy()
        self.train_stocker.clear()
        self.train_afficher.clear()
        return trains

    def affiche_train(self, fenetre_saat, train, pos_x, pos_y):
        """
        Displays a train in the window.

        Args:
            fenetre_saat (SAATWindow): The SAATWindow object.
            train (str): The train to display.
            pos_x (int): The x position where to display the train.
            pos_y (int): The y position where to display the train.

        Returns:
            None
        """
        color = self.index_indetermine
        try:
            if int(train[0]) % 2 == 0:
                color = self.index_pair
            else:
                color = self.index_impair
        except ValueError:
            pass
        draw_text_in_boxes(train, fenetre_saat, COULEUR_BACK, color, pos_x, pos_y)

    def affiche_fenetre(self, list_fenetre_saat):
        """
        Displays the window.

        Args:
            list_fenetre_saat (List[SAATWindow]): The list of SAATWindow objects.

        Returns:
            None
        """
        if list_fenetre_saat[self.ecran - 1].vide == 0:
            self.affiche_fenetre_invisible(list_fenetre_saat)
        else:
            self.affiche_fenetre_visible(list_fenetre_saat)

    def affiche_fenetre_visible(self, list_fenetre_saat):
        """
        Displays the visible part of the window.

        Args:
            list_fenetre_saat (List[SAATWindow]): The list of SAATWindow objects.

        Returns:
            None
        """
        for ligne in self.pos:
            text_vide = "      "
            pos_x = int(ligne[1]) - 1
            pos_y = int(ligne[0]) - 1
            draw_text_in_boxes(text_vide, list_fenetre_saat[self.ecran - 1], COULEUR_BACK, COULEUR_BACK, pos_x, pos_y)

        for compteur, train in enumerate(self.train_afficher):
            if compteur < self.nb_ligne:
                pos_x = int(self.pos[compteur][1]) - 1
                pos_y = int(self.pos[compteur][0]) - 1
                self.affiche_train(list_fenetre_saat[self.ecran - 1], train, pos_x, pos_y)

        if self.vdb is not None:
            symbol = "*" if len(self.train_afficher) > self.nb_ligne else " "
            pos_x = self.vdb[1] - 1
            pos_y = self.vdb[0] - 1
            draw_text_in_boxes(symbol, list_fenetre_saat[self.ecran - 1], COULEUR_BACK, COULEUR_VDB, pos_x, pos_y)

    def affiche_fenetre_invisible(self, list_fenetre_saat):
        """
        Displays the invisible part of the window.

        Args:
            list_fenetre_saat (List[SAATWindow]): The list of SAATWindow objects.

        Returns:
            None
        """
        margin = 20
        for ligne in self.pos:
            pos_x = int(ligne[1]) + margin
            pos_y = int(ligne[0]) - 1
            draw_text_in_boxes("      ", list_fenetre_saat[self.ecran - 1], COULEUR_BACK, COULEUR_BACK, pos_x, pos_y)

        for compteur, train in enumerate(self.train_afficher):
            if compteur < self.nb_ligne:
                pos_x = int(self.pos[compteur][1]) + margin
                pos_y = int(self.pos[compteur][0]) - 1
                self.affiche_train(list_fenetre_saat[self.ecran - 1], train, pos_x, pos_y)

        if self.vdb is not None:
            pos_x = self.vdb[1] + margin + 6
            pos_y = self.vdb[0] - 1
            symbol = "*" if len(self.train_afficher) > self.nb_ligne else " "
            draw_text_in_boxes(symbol, list_fenetre_saat[self.ecran - 1], COULEUR_BACK, COULEUR_VDB, pos_x, pos_y)

    def ini_fenetre_invisible(self, list_fenetre_saat):
        """
        Initializes the invisible window.

        Args:
            list_fenetre_saat (List[SAATWindow]): The list of SAATWindow objects.

        Returns:
            None
        """
        if list_fenetre_saat[self.ecran - 1].vide == 0:
            pos_x = int(self.pos[0][1]) - 1
            pos_y = int(self.pos[0][0]) - 1
            name = "FENETRE INVISIBLE " + str(self.num)
            draw_text_in_boxes(name, list_fenetre_saat[self.ecran - 1], Qt.green, COULEUR_BACK, pos_x, pos_y)

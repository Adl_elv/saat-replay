import re

from module.Color import color_map
from module.Fenetre import Fenetre


def read_ext_saat_p(file):
    """
    Reads the contents of a file containing external SAATs and their IDs, and extracts the information
    into a dictionary.

    Arguments:
        file (file): The file object representing the saat file to be read.

    Returns:
        dict: A dictionary containing the extracted SAAT information. The keys are the names of the SAATs, and the
              values are the corresponding indexes.
    """
    saat_list = {}

    # Reading the file
    for line in file:
        # Start of the relevant part
        if ";EQLIA" in line:
            next(file)
            # Reading the desired part
            for desired_line in file:
                formated_line = desired_line.strip()
                # Checking end of the relevant part
                if not formated_line or line.startswith(';NLIAI'):
                    break
                # Checking line to read
                elif formated_line[0] != ';' and formated_line[0] != '\n':
                    words = re.split('[: ]+', formated_line)
                    num_liaison = int(words[2][1], 16)
                    if num_liaison < 10:
                        saat_list[words[0]] = "0" + words[2][0] + "0" + str(num_liaison)
                    else:
                        saat_list[words[0]] = "0" + words[2][0] + str(num_liaison)
    # Declaring unassigned SAAT
    saat_list["SAAT(s) Inconnu(s)"] = None
    return saat_list


def read_ext_saat_s(file):
    """
    Reads the contents of a file containing external SAATs and their IDs, and extracts the information
    into a dictionary.

    Arguments:
        file (file): The file object representing the saat file to be read.

    Returns:
        dict: A dictionary containing the extracted SAAT information. The keys are the names of the SAATs, and the
              values are the corresponding indexes.
    """
    saat_list = {}

    # Reading the file
    for line in file:
        line = line.strip()
        # Checking for the end of the relevant part
        if not line:
            break
        # Checking the line to read
        elif line[0] != ';' and line[0] != '\n' and line[0] == 'C':
            words = line.split()
            num_liaison = int(words[2][1], 16)
            if num_liaison < 10:
                saat_list[words[0][1:]] = "0" + words[2][0] + "0" + str(num_liaison)
            else:
                saat_list[words[0][1:]] = "0" + words[2][0] + str(num_liaison)
    # Declaring unassigned SAAT
    saat_list["SAAT(s) Inconnu(s)"] = None
    return saat_list


def read_carac_saat(file):
    """
    Reads the contents of a carac-type file and extracts the values of all characters.

    Arguments:
        file (file): The file object representing the saat file to be read.

    Returns:
        list: A list containing the characters extracted from the carac file. Each element of the list is a
              list of values corresponding to the hexadecimal values of the character's image.
    """
    carac = []

    # Reading the file
    for line in file:
        # Selecting the content to check
        words = line.split()

        # Checking the desired line
        if len(words) > 1 and words[0] == "DEFB":
            carac.append(words[1].strip("\n").split(","))
    return carac


def extract_window(file, pattern, is_colored=False):
    """
    Extracts window information from a given file based on a regex pattern.

    Args:
        file (file): File object to read.
        pattern (str): Regex pattern to identify relevant lines.
        is_colored (bool): If True, extracts color indices, otherwise ignores them.

    Returns:
        list: List of extracted Fenetre objects.
    """

    # Compiling the regex pattern for searching
    compiled_pattern = re.compile(pattern)

    # List to store the found windows
    fenetres = []

    # Iterating through each line of the file
    for line in file:
        # Searching for the pattern in the line
        match = compiled_pattern.search(line)

        # If the pattern is found, start extracting window information
        if match:
            fenetre = Fenetre()
            fenetre.ecran = int(line[1])

            # Iterating through lines until finding "DEFB"
            for desired_line in file:

                if desired_line.split()[0] == "DEFB":
                    fenetre.num = int(desired_line.split()[1])
                    fenetre.nb_ligne = int(next(file).split()[1])
                    next(file)  # Skipping an unnecessary line

                    # If the file is colored, extract color indices
                    if is_colored:
                        fenetre.index_pair = color_map.get(next(file).strip().split()[1][:-1])
                        fenetre.index_impair = color_map.get(next(file).strip().split()[1][:-1])
                        fenetre.index_indeterminate = color_map.get(next(file).strip().split()[1][:-1])

                    # Extracting window coordinates
                    coords = next(file).split()[1].split(',')
                    fenetre.pos = [(int(coords[i]), int(coords[i + 1])) for i in range(0, len(coords), 2)]

                    # Converting coordinates to int and storing in fenetre.vdb
                    vdb_line = next(file).split()
                    if vdb_line and vdb_line[0] == 'DEFB':
                        fenetre.vdb = [int(coord) for coord in vdb_line[1].split(',')]

                    # Adding the window to the list of windows
                    fenetres.append(fenetre)
                    break
                else:
                    continue

    return fenetres


def read_window_line(file):
    """
    Reads the number of lines for each window from a file.

    Arguments:
        file (file): File object to read.

    Returns:
        list: A list containing the number of lines for each window.
    """
    nb_lignes = []

    # Reading the file
    for line in file:
        line = line.strip().split()
        # Checking if the line contains relevant data
        if line and line[0] == 'DEFB':
            value = line[1].split(',')
            # Adding the number of lines for the window
            if len(value) > 1:
                nb_lignes.append(int(value[-1]))
    return nb_lignes


def read_window(file_fenetre, list_fenetre_saat_interne):
    """
    Reads window information from a file.

    Args:
        file_fenetre (file): File object to read.
        list_fenetre_saat_interne (List[SAATWindow]): List of internal SAAT windows.

    Returns:
        list: List of extracted Fenetre objects.
    """
    try:
        # Attempt to extract window information with color indices
        res = extract_window(file_fenetre, r'T\d+FE\d+:', True)
        return res
    except IndexError:
        # Resize internal SAAT windows and extract window information without color indices
        for fenetre in list_fenetre_saat_interne:
            fenetre.resize(1298, 609)
            fenetre.ui.scene.setSceneRect(0, 0, 1280, 528)
        res = extract_window(file_fenetre, r'T\d+FE\d+:', False)
        return res

import sys
import os

from module.FileReader import read_carac_saat, read_window, read_window_line, read_ext_saat_p, read_ext_saat_s
from module.SAATExtWindow import SAATExtWindow
from module.SAATWindow import SAATWindow


# Constantes pour la définition du format
FORMAT_STRINGS = {
    'FORMAT 24 LIGNES  ET 80 COLONNES': 0,
    'FORMAT 48 LIGNES  ET 80 COLONNES': 1,
}


def find_file(folder_path, file_name):
    """
    Searches for a specified file in a folder hierarchy, case-insensitive.

    Args:
        folder_path (str): Path of the root directory of the folder hierarchy to be traversed.
        file_name (str): Name of the file to search for.

    Returns:
        str or None: Full path of the file if found, otherwise None.

    """
    # Create a case-insensitive pattern to search for
    file_name_lower = file_name.lower()
    # Obtain a list of all files matching the pattern
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            # Filter the list to get files that exactly match the given file name
            if file.lower() == file_name_lower:
                # If matching files are found, return the first one. Otherwise, return None.
                return os.path.join(root, file)
    return None


def open_file(file_path, mode='r', encoding='ISO-8859-1'):
    """
    Opens a file with specific access rights.

    Arguments:
        file_path (string): The path to the file to be opened.
        mode (string): The file access mode (default is 'r' for read).
        encoding (string): The file encoding (default is 'ISO-8859-1').

    Returns:
        file: A file object that can be used to read or write data.
    """
    try:
        return open(file_path, mode, encoding=encoding)
    except FileNotFoundError:
        print(f"File {file_path} not found.")
        sys.exit(1)  # exits the program with an error code
    except PermissionError:
        print(f"Permission denied for reading the file {file_path}.")
        sys.exit(1)  # exits the program with an error code


def lecture_archive(file_name):
    """
    Reads an archive file and returns its contents.

    Arguments:
        file_name (string): The name of the file to read.

    Returns:
        list: A list of the lines in the file.
    """
    if not file_name:
        return []
    # Opening the archive file
    with open_file(file_name) as file:
        return file.readlines()


def lecture_saat_ext(folder_path):
    """
    Reads external SAAT files and creates a dictionary and a window for external SAATs.

    Arguments:
        folder_path (string): Path to the folder containing the TRAME.SRC file.

    Returns:
        tuple: A tuple containing the dictionary of external SAATs and the window for external SAATs.
    """
    try:
        file_path = find_file(folder_path, 'PARSAAT2.SRC')
        with open(file_path, 'r', encoding='ISO-8859-1') as file:
            # Creating a dictionary linking external SAAT and index
            dict_saat_ext = read_ext_saat_p(file)
            # Creating the window for external SAATs
            fenetre_saat_ext = SAATExtWindow(dict_saat_ext)
            return dict_saat_ext, fenetre_saat_ext

    except TypeError:
        try:
            file_path = find_file(folder_path, 'TLIBEC.SRC')
            with open(file_path, 'r', encoding='ISO-8859-1') as file:
                # Creating a dictionary linking external SAAT and index
                dict_saat_ext = read_ext_saat_s(file)
                # Creating the window for external SAATs
                fenetre_saat_ext = SAATExtWindow(dict_saat_ext)
                return dict_saat_ext, fenetre_saat_ext
        except TypeError:
            print(f"Files PARSAAT2.SRC and TLIBEC.SRC not found.")
            sys.exit(1)  # exits the program with an error code
    except PermissionError:
        print(f"Permission denied to read the TLIBEC.SRC or TLIBEC1.SRC file.")
        sys.exit(1)  # exits the program with an error code


def definition_carac(folder_path):
    """
    Reads the contents of a character file and determines the character format.

    Arguments:
        folder_path (string): Path to the folder containing the TRAME.SRC file.

    Returns:
        tuple: A tuple containing the list of characters and the color format.
    """
    file_path = find_file(folder_path, 'CARACT.DOC')
    # Opening the character definitions file
    with open_file(file_path) as file:
        return read_carac_saat(file)


def lecture_trame(folder_path, list_carac):
    """
    Reads the contents of a frame file, segments the file, and creates images from each segment.

    Arguments:
        folder_path (string): Path to the folder containing the TRAME.SRC file.
        list_carac (list[string]): Description of different characters.

    Returns:
        list: List of internal SAAT window images created from the segments of the frame file.
    """
    # Opening the file for creating the image
    file_path = find_file(folder_path, 'TRAME.SRC')
    if file_path is None:
        i = 1
        sections = []
        while True:
            file_path = find_file(folder_path, 'IMAGE' + str(i) + '.SRC')
            if file_path is None:
                sections.append("CFINEC")
                break
            with open_file(file_path) as file:
                sections.append(file.read() + "CFINEC")
            i += 1
    else:
        with open_file(file_path) as file:
            # Segmenting the file containing each defined image
            sections = [section + "CFINEC" for section in file.read().split("CFINEC")]
            if len(sections) == 1:
                sections = [section + "CFINEC" for section in sections[0].split("CFINTR")]

    internal_saat_windows = []
    for i in range(len(sections) - 1):  # For each section containing an image
        # Checking for the presence of an alarm zone in the image
        alarm_presence = 'TALARMES#' in sections[i]
        # Creating and adding the image
        internal_saat_windows.append(
            SAATWindow(sections[i], list_carac, str(i + 1), alarm_presence))
    return internal_saat_windows


def definition_fenetres(folder_path, list_fenetre_saat_interne):
    """
    Reads the contents of a windows file, creates windows based on color format, and initializes windows
    not present in the frame.

    Arguments:
        folder_path (string): Path to the folder containing the TRAME.SRC file.
        list_fenetre_saat_interne (List[SAATWindow]): List of internal SAAT windows.

    Returns:
        list: List of train windows.
    """
    # Opening the file for creating the windows
    file_path = find_file(folder_path, 'TDESIM.SRC')
    with open_file(file_path) as file:
        # Creating windows based on colors
        list_fenetre_train = read_window(file, list_fenetre_saat_interne)
    # Opening the file for the number of lines in the windows
    file_path = find_file(folder_path, 'TFEN.SRC')
    with open_file(file_path) as file:
        # Retrieving the number of lines per window
        list_nb_ligne_fenetre = read_window_line(file)
    # Initializing windows
    for i in range(len(list_fenetre_train)):
        list_fenetre_train[i].nb_ligne_max = int(list_nb_ligne_fenetre[i])
        list_fenetre_train[i].ini_fenetre_invisible(list_fenetre_saat_interne)
    return list_fenetre_train
